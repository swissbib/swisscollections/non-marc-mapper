# Non-marc Mapper

This project maps the data from non marc sources to Marc21 for further processing



## Integration Test with kafka in container

Launch the kafka cluster and create the topics

```
make up
```

Produce some test data

```
./scripts/produce-test-data.sh
```

Set the following environment variables

```
KAFKA_BOOTSTRAP_SERVERS=localhost:29092
TOPIC_IN=swisscollections-mongo-extracted
TOPIC_OUT=swisscollections-deduplicated
TOPIC_DELETES=swisscollection-deletes
APPLICATION_ID=non-marc-mapper-test-2024-06-28
```

Run the program (click the green arrow in Main.scala in IntelliJ) or
```
sbt run
```

Consume the data (for example directly in intelliJ using Big Data Tools plugin)

