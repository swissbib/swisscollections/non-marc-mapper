import Dependencies._

ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  }
  else {
    None
  }
}



lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Non-marc Mapper",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      case "module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.swisscollections.App"),
    libraryDependencies ++= Seq(
      jodaTime,
      kafkaClients,
      kafkaStreams,
      kafkaStreamsTestUtils,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      scalatic,
      scalaTest % Test,
      scala_xml
    )
  )

