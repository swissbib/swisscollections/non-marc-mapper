echo "Waiting for Kafka to come online..."

cub kafka-ready -b kafka:29092 1 20

# delete topics
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-mongo-extracted \
  --delete

kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-deduplicated \
  --delete

  kafka-topics \
    --bootstrap-server kafka:9092 \
    --topic swisscollections-deletes \
    --delete

# create topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-mongo-extracted \
  --replication-factor 1 \
  --partitions 4 \
  --create

# create topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-deduplicated \
  --replication-factor 1 \
  --partitions 4 \
  --create

# create topic
kafka-topics \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-deletes \
  --replication-factor 1 \
  --partitions 4 \
  --create

sleep infinity
