# load some data
docker-compose exec kafka bash -c "
  kafka-console-producer \
  --bootstrap-server kafka:9092 \
  --topic swisscollections-mongo-extracted \
  --property 'parse.key=true' \
  --property 'key.separator=|' < records.txt"
