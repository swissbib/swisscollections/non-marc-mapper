/*
 * non-marc-mapper
 * Copyright (C) 2022  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.Mappers

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex
import scala.xml.{Elem, Node, NodeBuffer, NodeSeq}

class CmiMapper extends Mapper {
  private lazy val role = Map (
    "" -> "ctb",
    "Abgebildet (dpc)" -> "dpc",
    "Adressat (rcp)" -> "rcp",
    "Annotierende Person (ann)" -> "ann",
    "Arrangeur (arr)" -> "arr",
    "Bestandsbildner" -> "cre",
    "Beteiligt" -> "ctb",
    "Bildhauer (scl)" -> "scl",
    "Drucker (prt)" -> "prt",
    "Druckgrafiker (prm)" -> "prm",
    "Erwähnt" -> "subject",
    "Filmemacher (fmk)" -> "fmk",
    "Fotograf (pht)" -> "pht",
    "Herausgebendes Organ (isb)" -> "isb",
    "Herausgeber (edt)" -> "edt",
    "Illustrator (ill)" -> "ill",
    "Komponist (cmp)" -> "cmp",
    "Künstler (art)" -> "art",
    "Librettist (lbt)" -> "lbt",
    "Mitwirkender (ctb)" -> "ctb",
    "Normerlassende Gebietskörperschaft (enj)" -> "enj",
    "Radierer (etr)" -> "etr",
    "Regisseur (drt)" -> "drt",
    "Schreiber (scr)" -> "scr",
    "Stecher (egr)" -> "egr",
    "Textdichter (lyr)" -> "lyr",
    "Verfasser (aut)" -> "aut",
    "Widmungsempfänger (dte)" -> "dte",
    "Zusammenstellender (com)" -> "com",
    "Übersetzer (trl)" -> "trl",
  )

  private lazy val level = Map (
    "Bestand" -> "Bestand=Fonds",
    "Teilbestand" -> "Teilbestand=Sub-fonds=Sous-fonds",
    "Serie" -> "Serie=Series=Série",
    "Teilserie" -> "Teilserie=Sub-Series=Sous-série",
    "Dossier" -> "Dossier=File",
    "Teildossier" -> "Teildossier=Sub-file=Sous-dossier",
    "Einzelstück" -> "Dokument=Item=Pièce",
  )

  private lazy val format = Map (
    "Abschrift" -> "t",
    "Autograf" -> "t",
    "Bildmaterial" -> "k",
    "Brief" -> "t",
    "Computerausdruck" -> "a",
    "Dia" -> "g",
    "Druckschrift" -> "a",
    "Filmmaterial" -> "g",
    "Fotografie" -> "k",
    "Fotonegativ" -> "k",
    "Manuskript" -> "t",
    "Musikdruck" -> "c",
    "Musikmanuskript" -> "d",
    "Objekt" -> "r",
    "Typoskript" -> "t"
  )

  private lazy val formatGndContent = Map (
    "Autograf" -> "Autograf",
    "Bildmaterial" -> "Bild",
    "Brief" -> "Briefsammlung",
    "Dia" -> "Fotografie",
    "Filmmaterial" -> "Film",
    "Fotografie" -> "Fotografie",
    "Fotonegativ" -> "Fotonegativ",
    "Manuskript" -> "Handschrift",
    "Musikmanuskript" -> "Musikhandschrift",
    "Plan" -> "Plan"
  )

  private lazy val collection = Map (
    "f107903b4f5243efb00a914128749c98" -> "Familienarchive",
    "6ce2f1af89704cc7981136d0a46c03d1" -> "Gesellschafts- und Firmenarchive",
    "a36cdf5e071b4939a52e2463b813bf41" -> "Zunftarchive",
    "219cb94bc8b44155893713431543b7f2" -> "Handschriften",
    "a34b5b66fc264641a386fa30a59516c4" -> "Autographensammlungen",
    "7105cbf9afa34a2ba58bc37943e3f17c" -> "Privatbibliotheken",
    "8173c342949d4e71919d2dc1e5421130" -> "Drehbuchsammlung",
    "5bd782a5e98c496c82427a303997d341" -> "Historische Findmittel"
  )

  private lazy val autographen = Map (
    /* Autographensammlung der Zentralbibliothek Zürich, 4bbfc907c34749d6beced7a5bc300b48 */
    "e4ab66e609e54ef39c79de03f995d221" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "dd7cbb764ae0469c89e4fa52fc46e677" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "cf1a9310d1254969b258c144a80f8165" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "d357909ed636442fbc9df086c1daf13e" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "e9c7f81b4a4c4dd6982668cc0ee26200" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "e20cd65189ed4ebbba037eaf0d089a0a" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "32a2e588a9fa4a3abfd4943ff0fc9120" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "6829d6d8761c49e78900d0c68e6871c1" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "24f1f8be692f4d67a77801f14c295035" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "60911fb21b644cfeb5c97d90c3740fa5" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "0da71ac04650441d8085934d3f15f03f" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "9e60a8e131254dc386b722c155fa2233" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "53eec3bfc52043c7b5e351875c3c66e5" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "2ea88c5198c442a69ed74c9c640a108b" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "0eba8f6ea5f44f848b0b3ed8d02d0637" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "1103e0ee229340388a74e9bbc73c1b29" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "d5a9a41ef4464271846b51974f28b888" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "e03fedf3ec4f47808d6d15f88439b466" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "86c1e663a8f646d2812b7442a8d2f070" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "3cd53596f4364a47ad5e90de277a69e5" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "9dba5112711c4b5cb4140fd687a1a79a" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "6a12716afa564d27bc1203028f9d58f6" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "4d2599d5bc3a48aaa8201b52472587ff" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    "c81037af3d854c108c8ca0e5677e2367" -> "Autogr ZB & Autogr ZB fol Autographensammlung der Zentralbibliothek Zürich",
    /*Autographensammlung von Johannes Girsberger (1808-1880), 6ea9f758eb2748f0ad9023dc97313e1d*/
    "48d7253b85354523b6a1b4d18c3a1e0d" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "9e3875a276314efdbd65f86663c9b200" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "2c391b3845f04790bcbce534ff165c6b" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "da1a2c7fbfad4c8289d1086a16545cd2" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "0e8a9f1559f54dbd8f408a5bb0cd911d" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "20e7dbd6ceeb4c3e99cabc382edc08ac" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "a80d956370e64edf8dbe544a684ae551" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "dd32b9315c1d443e8250c99c7dbb8f1e" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "55c5a0ce38c24d1ea8f0670257acd704" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "dd792001eb654ae3a61cdf14839f9078" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "50c401e6703f42388476bb5279ed0919" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "3faf041f88a745b392d47807c7e9519e" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "c30f13bc1a1848079e7dbf97e3e2f6db" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "5694744b969e44d1bfe6ae8b92338c39" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "baddfe7a01dc434f94795931ebc741f2" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "ab5ba831a9ab4116a08a34abfbcf6b07" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "4c77eacb2ddd4164a198dd8bd9b23a82" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "cfb568d84e0d4ed1b0acc0c80418cc96" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    "2a54ed83c7774692bf70fbfee3066c95" -> "Autogr Girsberger Autographensammlung von Johannes Girsberger (1808-1880)",
    /*Autographensammlung von Hans Konrad Ott-Usteri (1788-1872), 861ffe83a4144d9d9fe77f42d26ffc03 */
    "7428f5915ea64b4796bce3bfa3fc1858" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "508b4be05542494db83275395562e901" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "a7995d4bf52341a0a99dd0659986d2c8" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "67d2f2ebf9a44a72abd2596c1fc12f45" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "c94bf8caed224746a70a114237bf639a" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "471872e152d04a46a9e3904a6526a1bf" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "ad487a39b72d4e2dbca04d09c5884226" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "6d314e92e36e453ba62f1cd48116da98" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "e6e0073dca8045c0a0201bb7f4955f50" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "d3d2756529f248b8bd50c017d3657630" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "ef0f2724c8164ccc9a48f05abc35ca0f" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    "9b54baeaa3c44e79b93adfe7ba46f919" -> "Autogr Ott Autographensammlung von Hans Konrad Ott-Usteri (1788-1872)",
    /* Briefsammlung der Zentralbibliothek Zürich, 3aabfb35a67546d58ab3eb66d9a8de28*/
    "354eea6e41404bd6908259ae00529c3e" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "c1a6f90ff3444df78facc4c63d633e0b" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "f723c41a1b6a48879b466ff098642b46" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "557e4b42d3ee43588362f520fd73cd3d" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "268fb2119a6f492f8a5b94919abd4217" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "7e4114b2489849e0ab95ab621c4b1477" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "1b359653639345498c706867c23c873f" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "18117263bf52480faff5e58c11b8dda2" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "a2c1997625634ea99ae48135c0a49539" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "441a8e77ba3f4994b1e55703ed50b123" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "2654294cc5634a42935116e6801d7784" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "5343e178cd5148e3b9eb73896b93b60b" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "dc86909e62dc4c2699d9ec306a4d00c7" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "4e31f6066e4b4d5a9793b02e76ac5acf" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "05eb4b9d47f9450d90d53ab6d670e505" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "792c73d6c69042a9b18b981615efeb25" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "11a06db313f8471692211e7a56a459ec" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "52ed7b372499445282ac331dc3a73a4c" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "1d56718f337645eaa1e4e3510b0a9a18" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "f24329ebfecf437e8404b6c9816c15e3" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "9cc10dac905440b2b8be51b1ec4dafa2" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "40c9b2eb1942454cabc172b1b7fa04da" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "cadf8161a1aa4ed8b6592971ca9dd24b" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
    "b23b2b01165541578a57a4aaa757ea2e" -> "Ms Briefe & Ms Briefe fol Briefsammlung der Zentralbibliothek Zürich",
  )

  private lazy val autographenId = Map(
    /* Autographensammlung der Zentralbibliothek Zürich, 4bbfc907c34749d6beced7a5bc300b48 */
    "e4ab66e609e54ef39c79de03f995d221" -> "4bbfc907c34749d6beced7a5bc300b48",
    "dd7cbb764ae0469c89e4fa52fc46e677" -> "4bbfc907c34749d6beced7a5bc300b48",
    "cf1a9310d1254969b258c144a80f8165" -> "4bbfc907c34749d6beced7a5bc300b48",
    "d357909ed636442fbc9df086c1daf13e" -> "4bbfc907c34749d6beced7a5bc300b48",
    "e9c7f81b4a4c4dd6982668cc0ee26200" -> "4bbfc907c34749d6beced7a5bc300b48",
    "e20cd65189ed4ebbba037eaf0d089a0a" -> "4bbfc907c34749d6beced7a5bc300b48",
    "32a2e588a9fa4a3abfd4943ff0fc9120" -> "4bbfc907c34749d6beced7a5bc300b48",
    "6829d6d8761c49e78900d0c68e6871c1" -> "4bbfc907c34749d6beced7a5bc300b48",
    "24f1f8be692f4d67a77801f14c295035" -> "4bbfc907c34749d6beced7a5bc300b48",
    "60911fb21b644cfeb5c97d90c3740fa5" -> "4bbfc907c34749d6beced7a5bc300b48",
    "0da71ac04650441d8085934d3f15f03f" -> "4bbfc907c34749d6beced7a5bc300b48",
    "9e60a8e131254dc386b722c155fa2233" -> "4bbfc907c34749d6beced7a5bc300b48",
    "53eec3bfc52043c7b5e351875c3c66e5" -> "4bbfc907c34749d6beced7a5bc300b48",
    "2ea88c5198c442a69ed74c9c640a108b" -> "4bbfc907c34749d6beced7a5bc300b48",
    "0eba8f6ea5f44f848b0b3ed8d02d0637" -> "4bbfc907c34749d6beced7a5bc300b48",
    "1103e0ee229340388a74e9bbc73c1b29" -> "4bbfc907c34749d6beced7a5bc300b48",
    "d5a9a41ef4464271846b51974f28b888" -> "4bbfc907c34749d6beced7a5bc300b48",
    "e03fedf3ec4f47808d6d15f88439b466" -> "4bbfc907c34749d6beced7a5bc300b48",
    "86c1e663a8f646d2812b7442a8d2f070" -> "4bbfc907c34749d6beced7a5bc300b48",
    "3cd53596f4364a47ad5e90de277a69e5" -> "4bbfc907c34749d6beced7a5bc300b48",
    "9dba5112711c4b5cb4140fd687a1a79a" -> "4bbfc907c34749d6beced7a5bc300b48",
    "6a12716afa564d27bc1203028f9d58f6" -> "4bbfc907c34749d6beced7a5bc300b48",
    "4d2599d5bc3a48aaa8201b52472587ff" -> "4bbfc907c34749d6beced7a5bc300b48",
    "c81037af3d854c108c8ca0e5677e2367" -> "4bbfc907c34749d6beced7a5bc300b48",
    /*Autographensammlung von Johannes Girsberger (1808-1880), 6ea9f758eb2748f0ad9023dc97313e1d*/
    "48d7253b85354523b6a1b4d18c3a1e0d" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "9e3875a276314efdbd65f86663c9b200" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "2c391b3845f04790bcbce534ff165c6b" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "da1a2c7fbfad4c8289d1086a16545cd2" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "0e8a9f1559f54dbd8f408a5bb0cd911d" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "20e7dbd6ceeb4c3e99cabc382edc08ac" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "a80d956370e64edf8dbe544a684ae551" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "dd32b9315c1d443e8250c99c7dbb8f1e" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "55c5a0ce38c24d1ea8f0670257acd704" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "dd792001eb654ae3a61cdf14839f9078" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "50c401e6703f42388476bb5279ed0919" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "3faf041f88a745b392d47807c7e9519e" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "c30f13bc1a1848079e7dbf97e3e2f6db" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "5694744b969e44d1bfe6ae8b92338c39" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "baddfe7a01dc434f94795931ebc741f2" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "ab5ba831a9ab4116a08a34abfbcf6b07" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "4c77eacb2ddd4164a198dd8bd9b23a82" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "cfb568d84e0d4ed1b0acc0c80418cc96" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    "2a54ed83c7774692bf70fbfee3066c95" -> "6ea9f758eb2748f0ad9023dc97313e1d",
    /*Autographensammlung von Hans Konrad Ott-Usteri (1788-1872), 861ffe83a4144d9d9fe77f42d26ffc03 */
    "7428f5915ea64b4796bce3bfa3fc1858" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "508b4be05542494db83275395562e901" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "a7995d4bf52341a0a99dd0659986d2c8" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "67d2f2ebf9a44a72abd2596c1fc12f45" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "c94bf8caed224746a70a114237bf639a" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "471872e152d04a46a9e3904a6526a1bf" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "ad487a39b72d4e2dbca04d09c5884226" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "6d314e92e36e453ba62f1cd48116da98" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "e6e0073dca8045c0a0201bb7f4955f50" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "d3d2756529f248b8bd50c017d3657630" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "ef0f2724c8164ccc9a48f05abc35ca0f" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    "9b54baeaa3c44e79b93adfe7ba46f919" -> "861ffe83a4144d9d9fe77f42d26ffc03",
    /* Briefsammlung der Zentralbibliothek Zürich, 3aabfb35a67546d58ab3eb66d9a8de28*/
    "354eea6e41404bd6908259ae00529c3e" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "c1a6f90ff3444df78facc4c63d633e0b" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "f723c41a1b6a48879b466ff098642b46" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "557e4b42d3ee43588362f520fd73cd3d" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "268fb2119a6f492f8a5b94919abd4217" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "7e4114b2489849e0ab95ab621c4b1477" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "1b359653639345498c706867c23c873f" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "18117263bf52480faff5e58c11b8dda2" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "a2c1997625634ea99ae48135c0a49539" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "441a8e77ba3f4994b1e55703ed50b123" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "2654294cc5634a42935116e6801d7784" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "5343e178cd5148e3b9eb73896b93b60b" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "dc86909e62dc4c2699d9ec306a4d00c7" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "4e31f6066e4b4d5a9793b02e76ac5acf" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "05eb4b9d47f9450d90d53ab6d670e505" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "792c73d6c69042a9b18b981615efeb25" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "11a06db313f8471692211e7a56a459ec" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "52ed7b372499445282ac331dc3a73a4c" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "1d56718f337645eaa1e4e3510b0a9a18" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "f24329ebfecf437e8404b6c9816c15e3" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "9cc10dac905440b2b8be51b1ec4dafa2" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "40c9b2eb1942454cabc172b1b7fa04da" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "cadf8161a1aa4ed8b6592971ca9dd24b" -> "3aabfb35a67546d58ab3eb66d9a8de28",
    "b23b2b01165541578a57a4aaa757ea2e" -> "3aabfb35a67546d58ab3eb66d9a8de28",
  )

  val mainRoles: Regex = "cre|scl|fmk|cmp|art|lbt|lyr|aut|com".r

  def mapToXmlMarc(recordAsXml: Elem): Elem = {
    val result =
      <record>
        {recordAsXml \\ "header"}
        <metadata>
          <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
            <leader>00000n{getFormat(recordAsXml)}m a2200205uc 4500</leader>
            <controlfield tag="001">ZBC{(recordAsXml \ "metadata" \ "Item" \ "Guid").text}</controlfield>
            {getDatestamp(recordAsXml)}
            <controlfield tag="008">{getProcessDate}{getDate(recordAsXml)}sz {getMaterialSpecific(recordAsXml)}{if ((recordAsXml \ "metadata" \ "Item" \ "Languages" \ "Language").nonEmpty && (recordAsXml \ "metadata" \ "Item" \ "Languages" \ "Language").head.text.nonEmpty) (recordAsXml \ "metadata" \ "Item" \ "Languages" \ "Language").head.text else "zxx"} d</controlfield>
            {getDoi(recordAsXml)}
            <datafield tag="035" ind1=" " ind2=" ">
              <subfield code="a">(ZBcollections){(recordAsXml \ "metadata" \ "Item" \ "Guid").text}</subfield>
            </datafield>
            <datafield tag="040" ind1=" " ind2=" ">
              <subfield code="a">CH-ZuSLS UZB ZB</subfield>
              <subfield code="b">ger</subfield>
            </datafield>
            {getLanguages(recordAsXml)}
            {getMainEntry(recordAsXml)}
            <datafield tag="245" ind1={if (getMainEntry(recordAsXml).nonEmpty) "1" else "0"} ind2="0">
              <subfield code="a">{(recordAsXml \ "metadata" \ "Item" \ "Titel").head.text}</subfield>
              {if ((recordAsXml \ "metadata" \ "Item" \ "Freitext3").nonEmpty) <subfield code="c">{(recordAsXml \ "metadata" \ "Item" \ "Freitext3").head.text}</subfield>}
            </datafield>
            {getProductionNotice(recordAsXml)}
            {getExtent(recordAsXml)}
            <datafield tag ="351" ind1=" " ind2=" ">
              <subfield code="c">{level((recordAsXml \ "metadata" \ "Item" \ "Objekttyp").head.text)}</subfield>
            </datafield>
            {getParent(recordAsXml, marcField="490")}
            {getField(recordAsXml, "ScopeAndContent", "500##a")}
            {getField(recordAsXml, "AccessInformation", "506##a")}
            {getField(recordAsXml, "PublicationNote", "5103#a")}
            {getField(recordAsXml, "FindingAid", "555##a")}
            {getField(recordAsXml, "Freitext7", "561##a")}
            {getTopics(recordAsXml)}
            {getContent(recordAsXml)}
            {getAddedEntry(recordAsXml)}
            {getSubjectEntry(recordAsXml)}
            {getLinkDigitized(recordAsXml)}
            {getLinkSupplements(recordAsXml)}
            {getParent(recordAsXml, marcField="830")}
            {getHolding(recordAsXml)}
            {getParent(recordAsXml, marcField = "990")}
          </marc:record>
        </metadata>
      </record>
    result
  }

  /**
   * Creates a marc field based on a record and cmiField
   * @param recordAsXml the source record
   * @param cmiField the name of the xml element to pick (for example ScopeAndContent)
   * @param marcField the name of the marc Field to create, this could contains indicators and subfields (for example 500##a)
   * @return an xml node, for example <datafield tag="500" ind1=" " ind2=" "><subfield code="a">picked value</subfield></datafield>
   */
  def getField(recordAsXml: Elem, cmiField: String, marcField: String): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ "metadata" \ "Item" \ cmiField

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    if (data.nonEmpty) {
      <datafield tag={field} ind1={firstInd} ind2={secInd}>
        <subfield code={subfield}>{data.head.text.replaceAll(": <a href=.*</a>", "").replaceAll("<a href=.*“>", "").replaceAll("<a href=.*\">", "").replaceAll("</a>", "")}</subfield>
      </datafield>
    }
    else {NodeSeq.Empty}
  }

  def getDate(recordAsXml: Elem): String = {
    val searchDate = recordAsXml \ "metadata" \ "Item" \ "SearchDate"

    if (searchDate.text.length == 4) "s" + searchDate.text + "    "
    else if (searchDate.nonEmpty && searchDate.text.length < 4) "s" + searchDate.text.reverse.padTo(4,"0").mkString.reverse
    else if (searchDate.nonEmpty && searchDate.text.takeRight(4) != "9999") "q" + searchDate.text.substring(0,4) + searchDate.text.takeRight(4)
    else "u1uuuuuuu"
  }

  def getHolding(recordAsXml: Elem): NodeSeq = {
    val availability = recordAsXml \ "metadata" \ "Item" \ "Freitext2"

    val libraryCode = {
      if (availability.nonEmpty && availability.head.text.contains("Handschriften- bzw. Musikabteilung")) "Z03"
      else if (availability.nonEmpty && availability.head.text.contains("Alte Drucke und Rara")) "Z06"
      else if (availability.nonEmpty && availability.head.text.contains("Graphische Sammlung und Fotoarchiv")) "Z02"
      else if (availability.nonEmpty && availability.head.text.contains("Handschriftenabteilung")) "Z03"
      else if (availability.nonEmpty && availability.head.text.contains("Musikabteilung")) "Z05"
      else if (availability.nonEmpty && availability.head.text.contains("Kartensammlung")) "Z04"
      else "Z01"
    }

    val callNumber = {
      if ((recordAsXml \ "metadata" \ "Item" \ "Signatur").nonEmpty) (recordAsXml \ "metadata" \ "Item" \ "Signatur").head.text
      else "-"
    }

    val holdingNote = {
      if (availability.nonEmpty) {
        if (availability.head.text.contains("Benutzung im Lesesaal") && availability.head.text.contains("(")) {
          val pattern = """Benutzung im Lesesaal.*\((.*)\)""".r
          val pattern(note) = availability.head.text
          if (note.nonEmpty) note else availability.head.text
        }
        else availability.head.text

      }
      else ""
    }

    val holdings = new NodeBuffer

    holdings += {
      <datafield tag="852" ind1="4" ind2=" ">
        <subfield code="b">{libraryCode}</subfield>
        <subfield code="c">ZBcollections</subfield>
        <subfield code="j">{callNumber}</subfield>
        {if (holdingNote.nonEmpty) <subfield code="z">{holdingNote}</subfield>}
      </datafield>
    }


    {
      if (availability.nonEmpty && availability.head.text.contains("Handschriften- bzw. Musikabteilung")) {
        holdings += {
          <datafield tag="852" ind1="4" ind2=" ">
            <subfield code="b">Z05</subfield>
            <subfield code="c">ZBcollections</subfield>
            <subfield code="j">{callNumber}</subfield>
            {if (holdingNote.nonEmpty) <subfield code="z">{holdingNote}</subfield> }
          </datafield>
        }
      }
    }

    holdings

  }

  def getLanguages(recordAsXml: Elem): NodeSeq = {
    val languages: NodeSeq = recordAsXml \ "metadata" \ "Item" \ "Languages" \ "Language"

    if (languages.length > 1) {
      <datafield tag="041" ind1=" " ind2=" ">
        {for (language <- languages) yield {
        if (language.head.text.nonEmpty) {
          <subfield code="a">{language.head.text}</subfield>
        }
      }}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getProductionNotice(recordAsXml: Elem): NodeSeq = {
    val creationDate = recordAsXml \ "metadata" \ "Item" \ "CreationDate"
    val places = recordAsXml \ "metadata" \ "Item" \ "Freitext4"

    val subfields = new NodeBuffer

    if (places.nonEmpty) {
      for (place <- places.head.text.split(";")) yield {
        subfields +=
          <subfield code ="a">{place.trim}</subfield>
      }
    }

    if (creationDate.nonEmpty | subfields.nonEmpty) {
      <datafield tag="264" ind1=" " ind2="0">
        {if (subfields.nonEmpty) subfields}
        {if (creationDate.nonEmpty) <subfield code="c">{creationDate.head.text}</subfield>}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getExtent(recordAsXml: Elem): NodeSeq = {
    val extent = (recordAsXml \ "metadata" \ "Item" \ "AssocFields3" \ "AssocField3").map(_.text)

    if (getField(recordAsXml, "Format", "300##a").nonEmpty) {
      getField(recordAsXml, "Format", "300##a")
    }
    else if (extent.nonEmpty) {
      <datafield tag="300" ind1=" " ind2=" ">
        <subfield code="a">{extent.mkString(" ; ")}</subfield>
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getLinkDigitized(recordAsXml: Elem): NodeSeq = {
    val link = recordAsXml \ "metadata" \ "Item" \ "LinkZuDigitalisat"

    if (link.nonEmpty && link.head.text.contains("a href")) {
      val UrlPattern = "<a href=\"?([^\"]*)\"? target.*".r
      val UrlPattern(url) = link.head.text

      if (url.nonEmpty) {
        <datafield tag="856" ind1="4" ind2="1">
          <subfield code="u">{url}</subfield>
          <subfield code="z">Link zu Digitalisat</subfield>
        </datafield>
          <datafield tag="900" ind1=" " ind2=" ">
            <subfield code="a">ZBConline</subfield>
          </datafield>
      }
      else NodeSeq.Empty
    }
    else if (link.nonEmpty) {
      val url = link.head.text

      <datafield tag="856" ind1="4" ind2="1">
        <subfield code="u">
          {url}
        </subfield>
        <subfield code="z">Link zu Digitalisat</subfield>
      </datafield>
        <datafield tag="900" ind1=" " ind2=" ">
          <subfield code="a">ZBConline</subfield>
        </datafield>

    }
    else NodeSeq.Empty
  }

  def getDoi(recordAsXml: Elem): NodeSeq = {
    val link = recordAsXml \ "metadata" \ "Item" \ "LinkZuDigitalisat"

    if (link.nonEmpty) {
      if (link.head.text.contains("doi") && link.head.text.contains("a href")) {
        val doiPattern = "<a href=\"?http.*doi.org/([^\"]*)\"? target.*".r
        val doiPattern(doi) = link.head.text

        <datafield tag="024" ind1="7" ind2=" ">
          <subfield code="a">{doi}</subfield>
          <subfield code="2">doi</subfield>
        </datafield>
      }
      else if (link.head.text.contains("doi")) {
        val doiPattern = "http.*doi.org/([^\"]*)".r
        val doiPattern(doi) = link.head.text

        <datafield tag="024" ind1="7" ind2=" ">
          <subfield code="a">{doi}</subfield>
          <subfield code="2">doi</subfield>
        </datafield>
      }
      else NodeSeq.Empty
    }
    else NodeSeq.Empty
  }

  def getLinkSupplements(recordAsXml: Elem): NodeSeq = {
    val links = recordAsXml \ "metadata" \ "Item" \ "AssocFields5" \ "AssocField5"
    val fields = new NodeBuffer

    if (links.nonEmpty) {
      {
        for (link <- links) yield {
          val elements = link.head.text.split(" \\| ")
          if (elements.length == 3) {
            fields +=
            <datafield tag="856" ind1="4" ind2="2">
              <subfield code="u">
                {elements(2)}
              </subfield>
              <subfield code="z">{elements(1)}: {elements(0)}</subfield>
            </datafield>
          }
        }
      }
    }
    fields
  }

  def getParent(recordAsXml: Elem, marcField: String): NodeSeq = {
    val parentId = recordAsXml \ "metadata" \ "Item" \ "Freitext1"
    val parents = recordAsXml \ "metadata" \ "Item" \ "UebergeordneteStruktur"
    val parentTitle = if (parents.nonEmpty) {parents.head.text.split('/') takeRight 1} else ""
    val callNumber = recordAsXml \ "metadata" \ "Item" \ "Signatur"

    if (parentId.nonEmpty && parentTitle != "" && marcField == "490") {
      if (autographen contains parentId.head.text) {
        <datafield tag={marcField} ind1="1" ind2=" ">
          <subfield code="a">{autographen(parentId.head.text)}</subfield>
          {if (callNumber.nonEmpty) <subfield code="v">{callNumber.head.text}</subfield>}
        </datafield>
      }
      else {
        <datafield tag={marcField} ind1="1" ind2=" ">
          <subfield code="a">{parentTitle}</subfield>
          {if (callNumber.nonEmpty) <subfield code="v">{callNumber.head.text}</subfield>}
        </datafield>
      }
    }
    else if (parentId.nonEmpty && parentTitle != "" && marcField == "830") {
      if (autographen contains parentId.head.text) {
        <datafield tag={marcField} ind1=" " ind2="0">
          <subfield code="a">{autographen(parentId.head.text)}</subfield>
          {if (callNumber.nonEmpty) <subfield code="v">{replaceRomanNumerals(callNumber.head.text)}</subfield>}
          <subfield code="w">ZBC{autographenId(parentId.head.text)}</subfield>
        </datafield>
      }
      else {
        <datafield tag={marcField} ind1=" " ind2="0">
          <subfield code="a">{parentTitle}</subfield>
          {if (callNumber.nonEmpty) <subfield code="v">{replaceRomanNumerals(callNumber.head.text)}</subfield>}
          <subfield code="w">ZBC{parentId.head.text}</subfield>
        </datafield>
      }
    }
    else if (parentId.nonEmpty && parentTitle == "" && marcField == "990") {
      <datafield tag="990" ind1=" " ind2=" ">
        <subfield code="a">{collection.withDefaultValue("Personennachlässe")(parentId.text)}</subfield>
      </datafield>
    }
    else NodeSeq.Empty
  }

  def replaceRomanNumerals(input: String): String = {
    input.replace("XIII", "13").replace("VIII", "8").replace("XII", "12").replace("VII", "7").replace("XI", "11").replace("IX", "9").replace("III", "3").replace("VI", "6").replace("IV", "4").replace("II", "2").replace("I", "1").replace("X", "10").replace("V", "5")
  }

  def getTopics(recordAsXml: Elem): NodeSeq = {
    val topics = recordAsXml \ "metadata" \ "Item" \ "AssocFields2" \ "AssocField2"

    if (topics.nonEmpty) {
      {for (topic <- topics) yield {
        <datafield tag="690" ind1=" " ind2=" ">
          <subfield code="a">{topic.head.text}</subfield>
          <subfield code="2">han-A6</subfield>
        </datafield>
      }}
    }
    else NodeSeq.Empty
  }

  def getContent(recordAsXml: Elem): NodeSeq = {
    val formats = (recordAsXml \ "metadata" \ "Item" \ "AssocFields1" \ "AssocField1").map(_.text)
    val contents = new NodeBuffer

    if (formats.nonEmpty ) {
      {
        for (format <- formats) yield {
          if (formatGndContent.keysIterator.contains(format)) {
            contents +=
              <datafield tag="655" ind1=" " ind2="7">
                <subfield code="a">{formatGndContent(format)}</subfield>
                <subfield code="2">gnd-content</subfield>
              </datafield>
          }
          else if (format == "Typoskript") {
            contents +=
              <datafield tag="655" ind1=" " ind2="4">
                <subfield code="a">Typoskript</subfield>
              </datafield>
          }
        }
      }
    }

    contents
  }

  def getFormat(recordAsXml: Elem): String = {
    val formats = (recordAsXml \ "metadata" \ "Item" \ "AssocFields1" \ "AssocField1").map(_.text)

    /* according to priority list from ZB */

    if (formats contains "Musikmanuskript") "d"
    else if (formats contains "Brief") "t"
    else if (formats contains "Manuskriptkarte") "f"
    else if (formats contains "Manuskript") "t"
    else if (formats contains "Musikdruck") "c"
    else if (formats contains "Karte") "e"
    else if (formats contains "Filmmaterial") "g"
    else if (formats contains "Panorama") "e"
    else if (formats contains "Profil") "e"
    else if (formats contains "Bildmaterial") "k"
    else if (formats contains "Computerausdruck") "a"
    else if (formats contains "Typoskript") "t"
    else if (formats contains "Autograf") "t"
    else if (formats contains "Abschrift") "t"
    else if (formats contains "Druckschrift") "a"
    else if (formats contains "Tonträger") "j"
    else if (formats contains "Objekt") "r"
    else if (formats contains "Fotografie") "k"
    else if (formats contains "Dia") "g"
    else if (formats contains "Fotonegativ") "g"
    else if (formats contains "Plan") "e"
    else if (formats contains "Datenträger") "m"
    else if (formats contains "Reproduktion") "p"
    else "p"
  }

  def getMaterialSpecific(recordAsXml: Elem): String = {
    /* 008 18-34 for all values of LDR/06 */
    val format = getFormat(recordAsXml)
    val formats = (recordAsXml \ "metadata" \ "Item" \ "AssocFields1" \ "AssocField1").map(_.text)


    format match {
      case "t" => "||||| |||||||| ||"
      case "a" => "||||| |||||||| ||"
      case "f" => "|||||| |  || | ||"
      case "e" => "|||||| |  || | ||"
      case "j" => "nnnn|||||||||| | "
      case "k" => "nnn |     |    ||"
      case "g" =>
        if (formats contains "Dia" ) "||| |     |    s|"
        else if (formats contains "Fotonegativ") "||| |     |    f|"
        else "||| |     |    ||"
      case "c" => "|||||||||||||| | "
      case "d" => "|||||||||||||| | "
      case "m" => "    ||  | |      "
      case "p" => "     |           "
      case "r" => "nnn |     |    ||"
    }

  }

  def getMainEntry(recordAsXML: Elem): NodeSeq = {
    val mains = getEntries(recordAsXML, "main")
    val entries = new NodeBuffer

    if (mains.nonEmpty) {
      entries +=
        <datafield tag={getEntryTag(mains.head, "1")} ind1={if (getEntryTag(mains.head, "1") == "110") "2" else "1"} ind2=" ">
          <subfield code="a">{mains.head.getOrElse("RegisterName", "")}</subfield>
          {if (mains.head.getOrElse("RegisterGND", "") != "") <subfield code="0">(DE-588){mains.head.getOrElse("RegisterGND", "")}</subfield>}
          <subfield code="4">{role(mains.head.getOrElse("RegisterRole", ""))}</subfield>
        </datafield>
    }

    entries
  }

  def getAddedEntry(recordAsXML: Elem): NodeSeq = {
    val mains = getEntries(recordAsXML, "main")
    val others = getEntries(recordAsXML, "added")
    val entries = new NodeBuffer

    if (mains.nonEmpty) {
      for (main <- mains.tail) yield {
        entries +=
          <datafield tag={getEntryTag(main, "7")} ind1={if (getEntryTag(main, "7") == "710") "2" else "1"} ind2=" ">
            <subfield code="a">{main.getOrElse("RegisterName", "")}</subfield>
            {if (main.getOrElse("RegisterGND", "") != "") <subfield code="0">(DE-588){main.getOrElse("RegisterGND", "")}</subfield>}
            <subfield code="4">{role(main.getOrElse("RegisterRole", ""))}</subfield>
          </datafield>
      }
    }

    for (contributor <- others) yield {
      entries +=
        <datafield tag={getEntryTag(contributor, "7")} ind1={if (getEntryTag(contributor, "7") == "710") "2" else "1"} ind2=" ">
          <subfield code="a">{contributor.getOrElse("RegisterName", "")}</subfield>
          {if (contributor.getOrElse("RegisterGND", "") != "") <subfield code="0">(DE-588){contributor.getOrElse("RegisterGND", "")}</subfield>}
          <subfield code="4">{role(contributor.getOrElse("RegisterRole", ""))}</subfield>
        </datafield>
    }

    entries
  }

  def getSubjectEntry(recordAsXML: Elem): NodeSeq = {
    val subjects = getEntries(recordAsXML, "subject")
    val entries = new NodeBuffer

    for (subject <- subjects) yield {
      entries +=
        <datafield tag={getEntryTag(subject, "6")} ind1={if (getEntryTag(subject, "6") == "610") "2" else "1"} ind2={if (subject.getOrElse("RegisterGND", "") != "") "7" else "4"}>
          <subfield code="a">{subject.getOrElse("RegisterName", "")}</subfield>
          {if (subject.getOrElse("RegisterGND", "") != "") <subfield code="0">(DE-588){subject.getOrElse("RegisterGND", "")}</subfield>
          <subfield code="2">gnd</subfield>
          }
        </datafield>
    }

    entries
  }

  def getEntries(recordAsXML: Elem, entry: String): ListBuffer[Map[String, String]] = {
    val contributors: Seq[Map[String, String]] = getContributors(recordAsXML)

    val mains = new ListBuffer[Map[String, String]]
    val subjects = new ListBuffer[Map[String, String]]
    val others = new ListBuffer[Map[String, String]]

    for (contributor <- contributors) role(contributor.getOrElse("RegisterRole", "")) match {
      case mainRoles() => mains += contributor
      case "subject" => subjects += contributor
      case _ => others += contributor
    }

    if (entry == "main") mains
    else if (entry == "added") others
    else subjects
  }

  def getContributors(recordAsXml: Elem): Seq[Map[String, String]] = {
    val registers: NodeSeq = (recordAsXml \ "metadata" \ "Item" \ "Beteiligte" \ "Register")

    val contributors = for (register <- registers) yield {
      register match {
        case <Register>{children @ _*}</Register> => getAsMap(children)
      }
    }
    contributors
  }

  def getAsMap(children: Seq[Node]): Map[String, String] = {
    var res = Map[String, String]()
    for (c <- children) yield {
      res += (c.label -> c.text)
    }
    res
  }

  def getEntryTag(contributor: Map[String, String], firstNumber: String): String = {
    firstNumber +
      {
        if (role(contributor.getOrElse("RegisterRole", "")) == "isb") "10"
        else if (role(contributor.getOrElse("RegisterRole", "")) == "enj") "10"
        else if (contributor.getOrElse("RegisterName", "") contains ", ") "00"
        else "10"
      }
  }

}
