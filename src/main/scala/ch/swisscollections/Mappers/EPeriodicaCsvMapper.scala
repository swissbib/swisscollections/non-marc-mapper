/*
 * Non-marc Mapper
 * Copyright (C) 2024  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.swisscollections.Mappers

import scala.xml.{Elem, NodeBuffer, NodeSeq}

class EPeriodicaCsvMapper extends Mapper {
  def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      <metadata>
        <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
          <leader>00000nam a2200205uc 4500</leader>
          <controlfield tag="001">EP{(recordAsXml \ "code").text.replace(":", "_")}</controlfield>
          <controlfield tag="008">{getProcessDate}s{(recordAsXml \ "date").text}    sz ||||| |||||||| ||{(recordAsXml \ "language").text.substring(0,3)} d</controlfield>
          <datafield tag="035" ind1=" " ind2=" ">
            <subfield code="a">(EP){(recordAsXml \ "code").text}</subfield>
          </datafield>
          {getLanguage(recordAsXml)}
          {getField(recordAsXml, "title", "245##a")}
          {getField(recordAsXml, "date", "264#1c")}
          <datafield tag="347" ind1=" " ind2=" ">
            <subfield code="a">text</subfield>
            <subfield code="b">pdf</subfield>
          </datafield>
          <datafield tag="540" ind1=" " ind2=" ">
            <subfield code="a">Urheberrechtsschutz (In Copyright)</subfield>
            <subfield code="u">https://rightsstatements.org/page/InC/1.0/</subfield>
          </datafield>
          <datafield tag="655" ind1=" " ind2="7">
            <subfield code="a">Zeitschrift/Heft</subfield>
            <subfield code="2">bgs-genre</subfield>
          </datafield>
          {getEditors(recordAsXml)}
          {getHost(recordAsXml)}
          {getField(recordAsXml, "linktoviewer", "85640u", "3", "online")}
          {getField(recordAsXml, "linktodocument", "85640u", "3", "pdf")}
          {getField(recordAsXml, "preview", "85648u", "3", "thumbnail iiif")}
          <datafield tag="859" ind1=" " ind2=" ">
            <subfield code="a">ep</subfield>
            <subfield code="b">{(recordAsXml \ "collection").text}</subfield>
            <subfield code="c">ep-{(recordAsXml \ "ep_code").text}</subfield>
            <subfield code="u">https://www.e-periodica.ch/</subfield>
          </datafield>
        </marc:record>
      </metadata>
    </record>
  }

  def getLanguage(recordAsXml: Elem): NodeSeq = {
    val languages = (recordAsXml \ "language").text.split(";")
    val field = new NodeBuffer

    if (languages.length > 1) {
      field +=
        <datafield tag="041" ind1=" " ind2=" ">
          {for (language <- languages) yield {
          <subfield code="a">{language.trim}</subfield>
        }}
        </datafield>
    }
    field
  }

  def getHost(recordAsXml: Elem): NodeSeq = {
    val title = recordAsXml \ "worktitle"
    val volume = recordAsXml \ "volume"
    val issue = recordAsXml \ "issue"
    val date = recordAsXml \ "date"

    <datafield tag="773" ind1=" " ind2=" ">
      <subfield code="t">{title.text}</subfield>
      <subfield code="g">{if (volume.text != "0") volume.text + ", "}{issue.text + " (" + date.text + ")"}</subfield>
    </datafield>

  }

  def getEditors(recordAsXml: Elem): NodeSeq = {
    val editors = (recordAsXml \ "editor").text.split("/")
    val field = new NodeBuffer

    if (editors.nonEmpty && editors(0) != "0") {
      for (editor <- editors) yield {
        field +=
          <datafield tag="710" ind1=" " ind2=" ">
            <subfield code="a">{editor.trim}</subfield>
            <subfield code="4">edt</subfield>
          </datafield>
      }
    }
    field
  }

  def getSplitField(recordAsXml: Elem, sourceField: String, marcField: String): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ sourceField

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    val fields = new NodeBuffer

    if (data.nonEmpty && data.text.contains(";")) {
      for (string <- data.text.split(";")) yield {
        fields +=
          <datafield tag={field} ind1={firstInd} ind2={secInd}>
            <subfield code={subfield}>{string.trim}</subfield>
            </datafield>
      }
    }
    else {
      fields +=
        <datafield tag={field} ind1={firstInd} ind2={secInd}>
          <subfield code={subfield}>{data.text}</subfield>
        </datafield>
    }

    fields
  }

  /**
   * Creates a marc field based on a record and a source Field
   * @param recordAsXml the source record
   * @param sourceField the name of the xml element to pick (for example ScopeAndContent)
   * @param marcField the name of the marc Field to create, this could contains indicators and subfields (for example 500##a)
   * @return an xml node, for example <datafield tag="500" ind1=" " ind2=" "><subfield code="a">picked value</subfield></datafield>
   */
  def getField(recordAsXml: Elem, sourceField: String, marcField: String, sourceSubfield: String = "", sourceCode: String = ""): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ sourceField

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    if (data.nonEmpty) {
      <datafield tag={field} ind1={firstInd} ind2={secInd}>
        <subfield code={subfield}>{data.head.text}</subfield>
        {if (sourceCode.nonEmpty)
        <subfield code={sourceSubfield}>{sourceCode}</subfield>
        }
      </datafield>
    }
    else {NodeSeq.Empty}
  }

}
