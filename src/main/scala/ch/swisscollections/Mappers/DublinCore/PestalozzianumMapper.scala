package ch.swisscollections.Mappers.DublinCore

import scala.xml.{Elem, NodeBuffer, NodeSeq}

class PestalozzianumMapper extends DublinCoreMapper {

  private lazy val collection = Map (
    "117656" -> "kjz",
    "14250" -> "swb",
    "55295" -> "gld",
    "117654" -> "arc",
  )

  private lazy val subCollection = Map (
    "PKW" -> "pkw",
    "IIJ" -> "iij",
    "NLS" -> "nls",
    "Schulgeschichte" -> "sls",
  )

  private lazy val bgsGenres = Map (
    "Druckgraphik" -> "Druckgraphik",
    "Film(e)" -> "Film",
    "Glasdiapositiv; 8,5 x 10 cm" -> "Glasdia",
    "Glasdiapositiv; Masse des Bildträgers: 8,5 x 10 cm" -> "Glasdia",
    "Kartografisches Material" -> "Kartografisches Material",
    "Kinderzeichnung" -> "Zeichnung",
    "Kommentar" -> "Kommentar",
    "Plakat" -> "Plakat",
    "Schulwandbild" -> "Schulwandbild",
  )

  private lazy val bgsCollectionGenres = Map (
    "kjz" -> "Zeichnung",
    "swb" -> "Schulwandbild",
    "gld" -> "Glasdia",
    "arc" -> "Archivalien",
  )

  override def mapToXmlMarc(recordAsXml: Elem): Elem = {
    val collectionCode = collection.withDefaultValue("arc")((recordAsXml \ "header" \ "setSpec").text.substring(34))
    val callNumber = (recordAsXml \ "metadata" \ "dc" \ "identifier").filterNot(_.text.startsWith("https:")).text

    <record>
      {recordAsXml \\ "header"}
      <metadata>
        <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
          <leader>00000n{if (List("kjz", "swb", "gld").contains(collectionCode)) "g" else "a"}m a2200205uc 4500</leader>
          <controlfield tag="001">STP{(recordAsXml \ "header" \ "identifier").text.substring(34)}</controlfield>
          {getDatestamp(recordAsXml)}
          <controlfield tag="008">{getProcessDate}{getDate(recordAsXml)}sz ||||| |||||||| ||und d</controlfield>
          <datafield tag="035" ind1=" " ind2=" ">
            <subfield code="a">(STP){(recordAsXml \ "header" \ "identifier").text.substring(34)}</subfield>
          </datafield>
          <datafield tag="090" ind1=" " ind2=" ">
            <subfield code="a">Pestalozzianum</subfield>
            <subfield code="b">{callNumber}</subfield>
            <subfield code="u">{(recordAsXml \ "metadata" \ "dc" \ "identifier").filter(_.text.startsWith("https:")).text}</subfield>
          </datafield>
          {getTitle(recordAsXml)}
          {if (List("kjz", "arc").contains(collectionCode)) {getCreationNotice(recordAsXml)} else {getPublicationNotice(recordAsXml)}}
          {getExtent(recordAsXml)}
          <datafield tag="347" ind1=" " ind2=" ">
            {if (collectionCode == "arc") {
            <subfield code="a">text</subfield>
              <subfield code="b">pdf</subfield>
          }
          else {<subfield code="a">bild</subfield>}
            }
          </datafield>
          {getNotes(recordAsXml)}
          {getField(recordAsXml, "rights", "540##a")}
          {getSubjects(recordAsXml)}
          <datafield tag="655" ind1=" " ind2="7">
            <subfield code="a">{bgsCollectionGenres.withDefaultValue("")(collectionCode)}</subfield>
            <subfield code="2">bgs-genre</subfield>
          </datafield>
          {getAuthors(recordAsXml)}
          {getLinks(recordAsXml)}
          <datafield tag="859" ind1=" " ind2=" ">
            <subfield code="a">stp</subfield>
            <subfield code="b">stp-{collectionCode}</subfield>
            {if (subCollection.withDefaultValue("")(callNumber.takeWhile(_ != '_')).nonEmpty) <subfield code="c">stp-{collectionCode}-{subCollection(callNumber.takeWhile(_ != '_'))}</subfield>}
            <subfield code="u">https://sammlungen.pestalozzianum.ch/</subfield>
          </datafield>
        </marc:record>
      </metadata>
    </record>
  }

  override def getDate(recordAsXml: Elem): String = {
    val dates = (recordAsXml \ "metadata" \ "dc" \ "date").map(_.text.trim)
    val cleanDate = {
      if (dates.length == 2 && dates.head.matches("[1-9][0-9]{3}") && dates(1).matches("[1-9][0-9]{3}")) {
        if (dates.head <= dates(1)) dates.head
        else dates(1)
      }
      else if (dates.nonEmpty)
        dates.head.replace("–", "-").replace(" - ", "-").replace("/", "-").replace("[", "").replace("]", "").replace("ca. ", "").replace("vor ", "").replace("nach ", "")
      else ""
    }

    if (cleanDate.length == 4 && cleanDate.matches("[0-9]+")) "s" + cleanDate + "    "
    else if (cleanDate.length == 9 && cleanDate.matches("[0-9-]+")) "m" + cleanDate.replace("-", "")
    else "u1uuuuuuu"
  }

  override def getTitle(recordAsXml: Elem): NodeSeq = {
    val title = recordAsXml \ "metadata" \ "dc" \ "title"
    val genre = recordAsXml \ "metadata" \ "dc" \ "type"

    if (title.nonEmpty) {
      <datafield tag="245" ind1="1" ind2=" ">
        <subfield code="a">{title.head.text.split(" : ", 2)(0)}</subfield>
        {if (title.head.text.contains(" : ")) <subfield code="b">{title.head.text.split(" : ", 2)(1)}</subfield>}
      </datafield>
    } else if (genre.nonEmpty) {
      <datafield tag="245" ind1="1" ind2=" ">
        <subfield code="a">[{genre.head.text} ohne Titel]</subfield>
      </datafield>
    }
    else {
      <datafield tag="245" ind1="1" ind2=" ">
        <subfield code="a">[Ohne Titel]</subfield>
      </datafield>
    }
  }

  def getCreationNotice(recordAsXml: Elem): NodeSeq = {
    val dates = (recordAsXml \ "metadata" \ "dc" \ "date").map(_.text.trim)
    val publishers = recordAsXml \ "metadata" \ "dc" \ "publisher"
    val places = recordAsXml \ "metadata" \ "dc" \ "coverage"

    val date = {
      if (dates.length == 2 && dates.head.matches("[1-9][0-9]{3}") && dates(1).matches("[1-9][0-9]{3}")) {
        if (dates.head <= dates(1)) dates.head
        else dates(1)
      }
      else if (dates.nonEmpty)
        dates.head.replace("–", "-").replace(" - ", "-")
      else ""
    }

    val subfields = new NodeBuffer

    if (places.nonEmpty) {
      val uniquePlaces = places.map(_.text).toSet

      for (place <- uniquePlaces) yield {
        subfields +=
          <subfield code ="a">{place}</subfield>
      }
    }

    if (publishers.nonEmpty) {
      for (publisher <- publishers) yield {
        if (!publisher.text.startsWith("Das Dia stammt")) {
          subfields +=
            <subfield code ="b">{publisher.text}</subfield>
        }
      }
    }

    {if (date.nonEmpty) subfields += <subfield code="c">{date}</subfield>}
    {if (date.isEmpty) subfields += <subfield code="c">[Datum nicht ermittelbar]</subfield>}

    if (subfields.nonEmpty) {
      <datafield tag="264" ind1=" " ind2="0">
        {subfields}
      </datafield>
    }
    else NodeSeq.Empty
  }

  override def getPublicationNotice(recordAsXml: Elem): NodeSeq = {
    val dates = (recordAsXml \ "metadata" \ "dc" \ "date").map(_.text.trim)
    val publishers = recordAsXml \ "metadata" \ "dc" \ "publisher"
    val places = recordAsXml \ "metadata" \ "dc" \ "coverage"

    val date = {
      if (dates.length == 2 && dates.head.matches("[1-9][0-9]{3}") && dates(1).matches("[1-9][0-9]{3}")) {
        if (dates.head <= dates(1)) dates.head
        else dates(1)
      }
      else if (dates.nonEmpty)
        dates.head.replace("–", "-").replace(" - ", "-")
      else ""
    }

    val subfields = new NodeBuffer

    if (places.nonEmpty) {
      val uniquePlaces = places.map(_.text).toSet

      for (place <- uniquePlaces) yield {
        subfields +=
          <subfield code ="a">{place}</subfield>
      }
    }

    if (publishers.nonEmpty) {
      for (publisher <- publishers) yield {
        if (!publisher.text.startsWith("Das Dia stammt")) {
          subfields +=
            <subfield code ="b">{publisher.text}</subfield>
        }
      }
    }

    {if (date.nonEmpty) subfields += <subfield code="c">{date}</subfield>}
    {if (date.isEmpty) subfields += <subfield code="c">[Datum nicht ermittelbar]</subfield>}

    if (subfields.nonEmpty) {
      <datafield tag="264" ind1=" " ind2="1">
        {subfields}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getSubjects(recordAsXml: Elem): NodeSeq = {
    val subjects = recordAsXml \ "metadata" \ "dc" \ "subject"
    val fields = new NodeBuffer

    if (subjects.nonEmpty) {
      for (subject <- subjects) yield {
        if (subject.text.contains("(Lehrperson)")) {
          fields +=
            <datafield tag="700" ind1=" " ind2=" ">
              <subfield code="a">{subject.text.replace(" (Lehrperson)", "")}</subfield>
              <subfield code="4">tch</subfield>
            </datafield>
        }
        else if (subject.text.contains("(Schulhaus)")) {
          fields +=
            <datafield tag="710" ind1=" " ind2=" ">
              <subfield code="a">{subject.text.replace(" (Schulhaus)", "")}</subfield>
              <subfield code="4">schulhaus</subfield>
            </datafield>
        }
        else if (subject.text.contains("(Wettbewerb)")) {
          fields +=
            <datafield tag="711" ind1=" " ind2=" ">
              <subfield code="a">{subject.text.replace(" (Wettbewerb)", "")}</subfield>
              <subfield code="4">wettbewerb</subfield>
            </datafield>
        }
        else {
          fields +=
            <datafield tag="650" ind1=" " ind2="4">
              <subfield code="a">{subject.text.replace(" (Schlagwort)", "").replace(" (Referenz)", "")}</subfield>
            </datafield>
        }
      }
    }
    fields
  }

  def getNotes(recordAsXml: Elem): NodeSeq = {
    val notes = recordAsXml \ "metadata" \ "dc" \ "description"
    val splitNotes = notes.text.split("\\*").filter(_.nonEmpty)
    val fields = new NodeBuffer

    if (notes.nonEmpty) {
      for (note <- notes) yield {
        val splitNote = note.text.split("\\*").filter(_.nonEmpty)
        for (singleNote <- splitNote) yield {
          fields +=
            <datafield tag="500" ind1=" " ind2=" ">
              <subfield code="a">{singleNote.trim}</subfield>
            </datafield>
        }
      }
    }
    fields
  }

  def getGenre(recordAsXml: Elem): NodeSeq = {
    val genres = recordAsXml \ "metadata" \ "dc" \ "type"
    val fields = new NodeBuffer

    if (genres.nonEmpty) {
      for (genre <- genres) yield {
        val bgsGenre = bgsGenres.withDefaultValue("")(genre.text)
        if (bgsGenre.nonEmpty) {
          fields +=
            <datafield tag="655" ind1=" " ind2="7">
              <subfield code="a">{bgsGenre}</subfield>
              <subfield code="2">bgs-genre</subfield>
            </datafield>
        }
      }
    }

    fields
  }

  def getExtent(recordAsXml: Elem): NodeSeq = {
    val fields = recordAsXml \ "metadata" \ "dc" \ "format"
    val splitNotes = fields.map(_.text.split("\\*").map(_.trim).filterNot(_.contains("/")).filter(_.nonEmpty)).flatten

    if (splitNotes.nonEmpty) {
      <datafield tag="300">
        <subfield code="a">{splitNotes.mkString(" ; ")}</subfield>
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getLinks(recordAsXml: Elem): NodeSeq = {
    val thumbnail = recordAsXml \ "about" \\ "link" \ "@href"
    val rights = recordAsXml \ "metadata" \ "dc" \ "rights"
    val access =
      if (rights.nonEmpty && rights.head.text.contains("Gesperrt")) false
      else true

    val formats = recordAsXml \ "metadata" \ "dc" \ "format"
    val format = formats.filter(_.text.contains("/")).text

    val fields = new NodeBuffer
    if (thumbnail.nonEmpty && access) {
      fields +=
        <datafield tag="856" ind1="4" ind2="0">
          <subfield code="u">{(recordAsXml \ "metadata" \ "dc" \ "identifier").filter(_.text.startsWith("https:")).text}</subfield>
          <subfield code="3">online</subfield>
        </datafield>
      fields +=
        <datafield tag="856" in1="4" ind2="8">
          <subfield code="u">{thumbnail.head.text.replace("_142.jpg", "_141.jpg")}</subfield>
          <subfield code="3">thumbnail</subfield>
        </datafield>
      if (format == "application/pdf") {
        val url = (recordAsXml \ "metadata" \ "dc" \ "source").head.text
        val pdfUrl = "(.*)_(.*)".r.replaceAllIn(url, m => s"${m.group(1)}.pdf")
        fields +=
          <datafield tag="856" ind1="4" ind2="0">
            <subfield code="u">{pdfUrl}</subfield>
            <subfield code="3">pdf</subfield>
          </datafield>
      }
    } else {
      fields +=
        <datafield tag="856" ind1="4" ind2="0">
          <subfield code="u">{(recordAsXml \ "metadata" \ "dc" \ "identifier").filter(_.text.startsWith("https:")).text}</subfield>
          <subfield code="3">viewer</subfield>
        </datafield>
    }
    fields
  }
}
