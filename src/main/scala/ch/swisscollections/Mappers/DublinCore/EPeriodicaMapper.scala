package ch.swisscollections.Mappers.DublinCore

import scala.xml.{Elem, NodeBuffer, NodeSeq}

class EPeriodicaMapper extends DublinCoreMapper {

  private lazy val editor = Map(
    "aip-001" -> "Conférence intercantonale de l'instruction publique de la Suisse romande et du Tessin (CIIP)",
    "aip-002" -> "Conférence intercantonale de l'instruction publique de la Suisse romande et du Tessin (CIIP)",
    "aip-003" -> "Conférence intercantonale de l'instruction publique de la Suisse romande et du Tessin (CIIP)",
    "ani-001" -> "Verband Musikschulen Schweiz",
    "ani-002" -> "Verband Musikschulen Schweiz",
    "ass-001" -> "Kraft, J.J./Spengler, G./Straub, J.W./Heer, J.",
    "avo-001" -> "Erziehungsdirektion Kt. Zürich und Pestalozzianum Zürich",
    "bkp-001" -> "Schweizerisches Rotes Kreuz",
    "bkp-002" -> "Schweizerisches Rotes Kreuz",
    "bkp-003" -> "Schweizerischer Verband diplomierter Krankenschwestern und Krankenpfleger",
    "bpe-001" -> "Société fribourgeoise d'éducation et du Musée pédagogique",
    "bpe-002" -> "Société fribourgeoise d'éducation",
    "bpg-001" -> "Société pédagogique genevoise",
    "bsb-001" -> "Wiget, Theodor (1883–1889)/Wiget, Gustav (1890–1891)/Conrad, Paul (1890–1902)",
    "bsf-001" -> "B. Bach",
    "bso-001" -> "Schulblattverein (–1921)/Berner Lehrerverein (1921–)",
    "bso-002" -> "Schulblattverein (–1921)/Berner Lehrerverein (1921–)",
    "cbl-001" -> "Berner Lehrerverein",
    "cuv-001" -> "Schweizerischer Armenerzieherverein",
    "cuv-002" -> "Schweizerischer Armenerzieherverein/Schweizerischer Verein für Heimerziehung und Anstaltsleitung",
    "cuv-003" -> "Verein für schweizerisches Anstaltswesen",
    "cuv-004" -> "Verein für Schweizerisches Heim- und Anstaltswesen",
    "cuv-005" -> "Verein für Schweizerisches Heimwesen",
    "cuv-006" -> "Verein für Schweizerisches Heimwesen",
    "cuv-007" -> "Heimverband Schweiz",
    "cuv-008" -> "Curaviva – Verband Heime und Institutionen Schweiz",
    "edu-001" -> "Syndicat des enseignants romands",
    "erz-001" -> "Stadlin, Josephine",
    "esi-001" -> "Associazione degli amici dell’educazione del popolo ticinese",
    "fri-001" -> "Schweizerische Friedensgesellschaft",
    "gfs-001" -> "Schweizerische Gemeinnützige Gesellschaft",
    "iua-001" -> "Pestalozzianum Zürich",
    "jbl-001" -> "Bündner Lehrerverein",
    "jbl-002" -> "Bündner Lehrerverein",
    "jgs-001" -> "Schweizerische Gesellschaft für Schulgesundheitspflege",
    "jpz-001" -> "Schweizerische Permanente Schulausstellung",
    "jpz-002" -> "Pestalozzianum Zürich",
    "jus-001" -> "Schweizerische Konferenz der kantonalen Erziehungsdirektoren (EDK)",
    "jus-002" -> "Schweizerische Konferenz der kantonalen Erziehungsdirektoren (EDK)",
    "jus-003" -> "Schweizerische Konferenz der kantonalen Erziehungsdirektoren (EDK)",
    "mgf-001" -> "Office fédéral du sport OFSPO",
    "mgf-002" -> "Office fédéral du sport OFSPO",
    "mgf-003" -> "Office fédéral du sport OFSPO",
    "mgf-004" -> "Office fédéral du sport OFSPO",
    "mgf-005" -> "Office fédéral du sport OFSPO",
    "pbe-001" -> "Consortium der zürcherischen Lehrerschaft",
    "peb-001" -> "Schweizerische Permanente Schulausstellung (ab 1891 Pestalozzianum Zürich)",
    "pes-001" -> "Pestalozzianum Zürich",
    "phi-001" -> "Pestalozzianum Zürich",
    "phi-002" -> "Pädagogische Hochschule Zürich",
    "pio-001" -> "Schweizerische Permanente Schulausstellung in Bern",
    "pka-001" -> "Pro Juventute",
    "pka-002" -> "Pro Juventute",
    "ppr-001" -> "Statistisches Bureau des eidgenössischen Departement des Innern",
    "ppr-002" -> "Statistisches Bureau des eidgenössischen Departement des Innern",
    "sbo-001" -> "Kaltschmidt, Jacob Heinrich",
    "sch-001" -> "Erziehungsdirektion Kanton Zürich",
    "sch-002" -> "Bildungsdirektion Kanton Zürich",
    "scs-001" -> "Verein katholischer Lehrer und Schulmänner der Schweiz",
    "scs-002" -> "Verein katholischer Lehrer und Schulmänner der Schweiz/Verein katholischer Lehrerinnen der Schweiz/Katholischer Erziehungsverein der Schweiz",
    "scs-003" -> "Katholischer Lehrerverein der Schweiz/Christlicher Lehrer- und Erzieherverein der Schweiz",
    "sle-001" -> "Schweizerischer Lehrerinnenverein",
    "slz-001" -> "Dachverband Schweizer Lehrerinnen und Lehrer",
    "slz-002" -> "Dachverband Schweizer Lehrerinnen und Lehrer",
    "slz-003" -> "Dachverband Schweizer Lehrerinnen und Lehrer",
    "ssa-001" -> "Schweizerische Gemeinnützige Gesellschaft",
    "ssa-002" -> "Schweizerische Gemeinnützige Gesellschaft",
    "ssa-003" -> "Schweizerischer Lehrerverein",
    "ssa-004" -> "Verband Schweizerischer Privatschulen",
    "ssa-005" -> "Verband Schweizerischer Privatschulen",
    "syn-001" -> "Schulsynode des Kantons Zürich",
    "szb-001" -> "Schweizerische Gesellschaft für Bildungsforschung",
    "szb-002" -> "Schweizerische Gesellschaft für Bildungsforschung",
    "tpz-001" -> "Pestalozzianum Zürich",
    "vsb-001" -> "J.J. Vogt",
    "vsh-001" -> "Vereinigung der Schweizerischen Hochschuldozierenden",
    "vsh-002" -> "Vereinigung der Schweizerischen Hochschuldozierenden",
    "zrk-001" -> "Reallehrerkonferenz des Kantons Zürich",
    "zsk-001" -> "Sekundarlehrerkonferenz des Kantons Zürich",
    "zvl-001" -> "Schweizerische und deutsche Schulmänner",
  )

  private lazy val language = Map(
    "aip-001" -> "fre",
    "aip-002" -> "fre",
    "aip-003" -> "fre",
    "ani-001" -> "ger",
    "ani-002" -> "ger",
    "ass-001" -> "ger",
    "avo-001" -> "ger",
    "bkp-001" -> "ger;fre",
    "bkp-002" -> "ger;fre",
    "bkp-003" -> "ger;fre",
    "bpe-001" -> "fre",
    "bpe-002" -> "fre",
    "bpg-001" -> "fre",
    "bsb-001" -> "ger",
    "bsf-001" -> "ger",
    "bso-001" -> "ger",
    "bso-002" -> "ger",
    "cbl-001" -> "und",
    "cuv-001" -> "ger",
    "cuv-002" -> "ger",
    "cuv-003" -> "ger",
    "cuv-004" -> "ger",
    "cuv-005" -> "ger",
    "cuv-006" -> "ger",
    "cuv-007" -> "ger",
    "cuv-008" -> "ger",
    "edu-001" -> "fre",
    "erz-001" -> "ger",
    "esi-001" -> "ita",
    "frei-001" -> "ger",
    "gfs-001" -> "ger",
    "iua-001" -> "ger",
    "jbl-001" -> "ger",
    "jbl-002" -> "ger",
    "jgs-001" -> "und",
    "jpz-001" -> "ger",
    "jpz-002" -> "ger",
    "jus-001" -> "ger",
    "jus-002" -> "ger",
    "jus-003" -> "ger",
    "mgf-001" -> "fre",
    "mgf-002" -> "fre",
    "mgf-003" -> "fre",
    "mgf-004" -> "fre",
    "mgf-005" -> "fre",
    "pbe-001" -> "ger",
    "peb-001" -> "ger",
    "pes-001" -> "ger",
    "phi-001" -> "ger",
    "phi-002" -> "ger",
    "pio-001" -> "ger",
    "pka-001" -> "ger",
    "pka-002" -> "ger",
    "ppr-001" -> "ger;fre",
    "ppr-002" -> "ger",
    "sbo-001" -> "ger",
    "sch-001" -> "ger",
    "sch-002" -> "ger",
    "scp-001" -> "ger",
    "scs-001" -> "ger",
    "scs-002" -> "ger",
    "scs-003" -> "ger",
    "sle-001" -> "ger",
    "slz-001" -> "ger",
    "slz-002" -> "ger",
    "slz-003" -> "ger",
    "ssa-001" -> "ger",
    "ssa-002" -> "ger",
    "ssa-003" -> "ger",
    "ssa-004" -> "und",
    "ssa-005" -> "und",
    "syn-001" -> "ger",
    "szb-001" -> "und",
    "szb-002" -> "und",
    "tpz-001" -> "ger",
    "vsb-001" -> "ger",
    "vsh-001" -> "und",
    "vsh-002" -> "und",
    "zrk-001" -> "ger",
    "zsk-001" -> "ger",
    "zvl-001" -> "ger",
  )

  private lazy val collection = Map(
    "aip-001" -> "ep-snb",
    "aip-002" -> "ep-snb",
    "aip-003" -> "ep-snb",
    "ani-001" -> "ep-snb",
    "ani-002" -> "ep-snb",
    "ass-001" -> "ep-stp",
    "avo-001" -> "ep-stp",
    "bkp-001" -> "ep-snb",
    "bkp-002" -> "ep-snb",
    "bkp-003" -> "ep-snb",
    "bpe-001" -> "ep-other",
    "bpe-002" -> "ep-other",
    "bpg-001" -> "ep-snb",
    "bsb-001" -> "ep-snb",
    "bsf-001" -> "ep-snb",
    "bso-001" -> "ep-snb",
    "bso-002" -> "ep-snb",
    "cbl-001" -> "ep-snb",
    "cuv-001" -> "ep-snb",
    "cuv-002" -> "ep-snb",
    "cuv-003" -> "ep-snb",
    "cuv-004" -> "ep-snb",
    "cuv-005" -> "ep-snb",
    "cuv-006" -> "ep-snb",
    "cuv-007" -> "ep-snb",
    "cuv-008" -> "ep-snb",
    "edu-001" -> "ep-snb",
    "erz-001" -> "ep-stp",
    "esi-001" -> "ep-snb",
    "fri-001" -> "ep-stp",
    "gfs-001" -> "ep-stp",
    "iua-001" -> "ep-stp",
    "jbl-001" -> "ep-snb",
    "jbl-002" -> "ep-snb",
    "jgs-001" -> "ep-snb",
    "jpz-001" -> "ep-stp",
    "jpz-002" -> "ep-stp",
    "jus-001" -> "ep-snb",
    "jus-002" -> "ep-snb",
    "jus-003" -> "ep-snb",
    "mgf-001" -> "ep-other",
    "mgf-002" -> "ep-other",
    "mgf-003" -> "ep-other",
    "mgf-004" -> "ep-other",
    "mgf-005" -> "ep-other",
    "pbe-001" -> "ep-snb",
    "peb-001" -> "ep-stp",
    "pes-001" -> "ep-stp",
    "phi-001" -> "ep-stp",
    "phi-002" -> "ep-stp",
    "pio-001" -> "ep-snb",
    "pka-001" -> "ep-stp",
    "pka-002" -> "ep-stp",
    "ppr-001" -> "ep-snb",
    "ppr-002" -> "ep-snb",
    "sbo-001" -> "ep-stp",
    "sch-001" -> "ep-stp",
    "sch-002" -> "ep-stp",
    "scp-001" -> "ep-stp",
    "scs-001" -> "ep-snb",
    "scs-002" -> "ep-snb",
    "scs-003" -> "ep-snb",
    "sle-001" -> "ep-snb",
    "slz-001" -> "ep-snb",
    "slz-002" -> "ep-snb",
    "slz-003" -> "ep-snb",
    "ssa-001" -> "ep-stp",
    "ssa-002" -> "ep-stp",
    "ssa-003" -> "ep-stp",
    "ssa-004" -> "ep-stp",
    "ssa-005" -> "ep-stp",
    "syn-001" -> "ep-stp",
    "szb-001" -> "ep-other",
    "szb-002" -> "ep-other",
    "tpz-001" -> "ep-stp",
    "vsb-001" -> "ep-snb",
    "vsh-001" -> "ep-other",
    "vsh-002" -> "ep-other",
    "zrk-001" -> "ep-stp",
    "zsk-001" -> "ep-stp",
    "zvl-001" -> "ep-stp",
  )

  override def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      {recordAsXml \\ "header"}
      <metadata>
        <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
          <leader>00000naa a2200205uc 4500</leader>
          <controlfield tag="001">EP{(recordAsXml \ "header" \ "identifier").text.substring(13).replace(":", "_")}</controlfield>
          {getDatestamp(recordAsXml)}
          <controlfield tag="008">{getProcessDate}{getDate(recordAsXml)}sz ||||| |||||||| ||{language.withDefaultValue("und")((recordAsXml \ "header" \ "identifier").text.substring(13,20)).substring(0,3)} d</controlfield>
          {getDoi(recordAsXml)}
          {getLanguage(recordAsXml)}
          {getDdc(recordAsXml)}
          <datafield tag="035" ind1=" " ind2=" ">
            <subfield code="a">(EP){(recordAsXml \ "header" \ "identifier").text.substring(13)}</subfield>
          </datafield>
          {getTitle(recordAsXml)}
          {getPublicationNotice(recordAsXml)}
          <datafield tag="347" ind1=" " ind2=" ">
            <subfield code="a">text</subfield>
            <subfield code="b">pdf</subfield>
          </datafield>
          {getAccessCondition(recordAsXml)}
          <datafield tag="655" ind1=" " ind2="7">
            <subfield code="a">Artikel (Zeitschrift/Periodikum)</subfield>
            <subfield code="2">bgs-genre</subfield>
          </datafield>
          {getAuthors(recordAsXml)}
          {getEditors(recordAsXml)}
          {getHost(recordAsXml)}
          {getLinks(recordAsXml)}
          {getThumbnail(recordAsXml)}
          <datafield tag="859" ind1=" " ind2=" ">
            <subfield code="a">ep</subfield>
            <subfield code="b">{collection((recordAsXml \ "header" \ "identifier").text.substring(13,20))}</subfield>
            <subfield code="c">ep-{(recordAsXml \ "header" \ "identifier").text.substring(13,20)}</subfield>
            <subfield code="u">https://www.e-periodica.ch/</subfield>
          </datafield>
        </marc:record>
      </metadata>
    </record>
  }

  def getDdc(recordAsXml: Elem): NodeSeq = {
    val classes = (recordAsXml \ "header" \ "setSpec").filter(_.text.startsWith("ddc:"))
    val fields = new NodeBuffer

    if (classes.nonEmpty) {
      for (ddc <- classes) yield {
        fields +=
          <datafield tag="082" ind1=" " ind2=" ">
            <subfield code="a">{ddc.text.stripPrefix("ddc:")}</subfield>
          </datafield>
      }
    }
    fields
  }

  def getLanguage(recordAsXml: Elem): NodeSeq = {
    val languages = language.withDefaultValue("und")((recordAsXml \ "header" \ "identifier").text.substring(13, 20)).split(";")
    val field = new NodeBuffer

    if (languages.length > 1) {
      field +=
        <datafield tag="041" ind1=" " ind2=" ">
          {for (language <- languages) yield {
          <subfield code="a">{language}</subfield>
        }}
        </datafield>
    }
    field
  }

  def getEditors(recordAsXml: Elem): NodeSeq = {
    val editors = editor.withDefaultValue("")((recordAsXml \ "header" \ "identifier").text.substring(13, 20)).split("/")
    val field = new NodeBuffer

    if (editors.nonEmpty && editors(0) != "") {
      for (editor <- editors) yield {
        field +=
          <datafield tag="710" ind1=" " ind2=" ">
            <subfield code="a">{editor}</subfield>
            <subfield code="4">edt</subfield>
          </datafield>
      }
    }
    field
  }

  def getAccessCondition(recordAsXml: Elem): NodeSeq = {
    val rights = (recordAsXml \ "metadata" \ "dc" \ "rights").filterNot(_.text.trim.isEmpty)
    val field = new NodeBuffer

    if (rights.nonEmpty) {
      for (right <- rights) yield {
        field +=
        <datafield tag="540" ind1=" " ind2=" ">
          <subfield code="a">{if (right.text.contains("rightsstatements.org/page/InC/1.0/")) "Urheberrechtsschutz (In Copyright)" else right.text}</subfield>
          <subfield code="u">{right.text}</subfield>
        </datafield>
      }
    }
    field
  }

  def getLinks(recordAsXml: Elem): NodeSeq = {
    val ids = (recordAsXml \ "metadata" \ "dc" \ "identifier").filterNot(_.text.trim.isEmpty)
    val links = new NodeBuffer

    if (ids.nonEmpty) {
      for (id <- ids) yield {
        if (id.text.contains("/digbib/view?")) {
          links +=
            <datafield tag="856" ind1="4" ind2="0">
              <subfield code="u">{id.text}</subfield>
              <subfield code="3">online</subfield>
            </datafield>
        }
        if (id.text.contains("type=pdf")) {
          links +=
            <datafield tag="856" ind1="4" ind2="0">
              <subfield code="u">{id.text}</subfield>
              <subfield code="3">pdf</subfield>
            </datafield>
        }
      }
    }
    links
  }

  def getThumbnail(recordAsXml: Elem): NodeSeq = {
    val relations = (recordAsXml  \ "metadata" \ "dc" \ "relation").filterNot(_.text.trim.isEmpty)
    val thumbnails = new NodeBuffer

    if (relations.nonEmpty) {
      for (relation <- relations) yield {
        println(relation.text)
        if (relation.text.startsWith("vignette : ")) {
          thumbnails +=
            <datafield tag="856" ind1="4" ind2="8">
              <subfield code="u">{relation.text.replace("vignette : ", "").replace("/full/full/0/default.jpg", "")}</subfield>
              <subfield code="3">thumbnail iiif</subfield>
            </datafield>
              }
      }
    }
    thumbnails
  }

  /**
   * Creates field 773
   * dc:source occurs up to 8 times in a record from e-periodica, with the following content:
   * 0 = journal title
   * 1 = ZDB ID
   * 2 = ISSN
   * 3 = volume
   * 4 = year
   * 5 = number
   * 6 = issue
   * 7 = pages
   * @param recordAsXml the source record
   * @return an xml node for field 773
   */
  def getHost(recordAsXml: Elem): NodeSeq = {
    val sources = recordAsXml \ "metadata" \ "dc" \ "source"

    if (sources.nonEmpty) {
      <datafield tag="773" ind1=" " ind2=" ">
        {if (sources.head.nonEmpty) <subfield code="t">{sources.head.text}</subfield>}

        {if (sources.length >= 4) <subfield code="g">{getRelatedParts(sources).trim}</subfield>}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getRelatedParts(sources: NodeSeq): String = {
    if (sources.length == 8) {
      (if (sources(3).text.trim.nonEmpty && sources(3).text != "-") sources(3).text else "") +
        (if (sources(5).text.trim.nonEmpty && (sources(3).text.trim.nonEmpty && sources(3).text != "-")) ", " + sources(5).text else if (sources(5).text.trim.nonEmpty && sources(5).text != "-" ) sources(5).text else "") +
        (if (sources(6).text.trim.nonEmpty && ((sources(3).text.trim.nonEmpty && sources(3).text != "-") || sources(5).text.trim.nonEmpty)) ", " + sources(6).text else if (sources(6).text.trim.nonEmpty) sources(6).text else "") +
        (if (sources(4).text.trim.nonEmpty && ((sources(3).text.trim.nonEmpty && sources(3).text != "-") || sources(5).text.trim.nonEmpty || sources(6).text.trim.nonEmpty)) " (" + sources(4).text + ")" else if (sources(4).text.trim.nonEmpty) sources(4).text else "") +
        (if (sources(7).text.trim.nonEmpty) ", S. " + sources(7).text else "")
    }
    else "not enough elements"
  }

}
