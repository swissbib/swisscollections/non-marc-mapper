/*
 * non-marc-mapper
 * Copyright (C) 2022  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.Mappers.DublinCore

import ch.swisscollections.Mappers.Mapper

import scala.annotation.unused
import scala.xml.{Elem, NodeBuffer, NodeSeq}

class DublinCoreMapper extends Mapper {

  def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      {recordAsXml \\ "header"}
      <metadata>
      </metadata>
    </record>
  }

  def getTitle(recordAsXml: Elem): NodeSeq = {
    val title = (recordAsXml \ "metadata" \ "dc" \ "title").head.text

    if (title.nonEmpty) {
      <datafield tag="245" ind1="1" ind2=" ">
        <subfield code="a">{title.split(" : ", 2)(0)}</subfield>
        {if (title.contains(" : ")) <subfield code="b">{title.split(" : ", 2)(1)}</subfield>}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getDoi(recordAsXml: Elem): NodeSeq = {
    val ids = (recordAsXml \ "metadata" \ "dc" \ "identifier").filter(_.text.startsWith("doi:"))
    val doi = new NodeBuffer

    if (ids.nonEmpty) {
      for (id <- ids) yield {
          doi +=
            <datafield tag="024" ind1="7" ind2=" ">
              <subfield code="a">{id.text.stripPrefix("doi:")}</subfield>
              <subfield code="2">doi</subfield>
            </datafield>
        }
    }
    doi
  }

  def getDate(recordAsXml: Elem): String = {
    val date = (recordAsXml \ "metadata" \ "dc" \ "date").head.text

    if (date.length == 4 && date.matches("[0-9]+")) "s" + date + "    "
    else if (date.length == 10 && date.matches("[0-9-]+")) "e" + date.replace("-", "")
    else "u1uuuuuuu"
  }

  def getPublicationNotice(recordAsXml: Elem): NodeSeq = {
    val date = (recordAsXml \ "metadata" \ "dc" \ "date").head.text
    val publishers = recordAsXml \ "metadata" \ "dc" \ "publisher"

    val subfields = new NodeBuffer

    if (publishers.nonEmpty) {
      for (publisher <- publishers) yield {
        subfields +=
          <subfield code ="b">{publisher.text}</subfield>
      }
    }

    if (date.nonEmpty | subfields.nonEmpty) {
      <datafield tag="264" ind1=" " ind2="1">
        {if (subfields.nonEmpty) subfields}
        {if (date.nonEmpty) <subfield code="c">{date}</subfield>}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getAuthors(recordAsXml: Elem): NodeSeq = {
    val creators = (recordAsXml \ "metadata" \ "dc" \ "creator").filterNot(_.text.trim.isEmpty)
    val contributors = (recordAsXml \ "metadata" \ "dc" \ "contributor").filterNot(_.text.trim.isEmpty)
    val both = creators ++ contributors
    val authors = new NodeBuffer

    if (both.nonEmpty) {
      for (author <- both) {
        if (author.text != "[s.n.]") {
          authors +=
            <datafield tag="700" ind1=" " ind2=" ">
              <subfield code="a">{author.text}</subfield>
            </datafield>
        }
      }
    }
    authors
  }

  /**
   * Creates a marc field based on a record and a source Field
   * @param recordAsXml the source record
   * @param sourceField the name of the xml element to pick (for example ScopeAndContent)
   * @param marcField the name of the marc Field to create, this could contains indicators and subfields (for example 500##a)
   * @return an xml node, for example <datafield tag="500" ind1=" " ind2=" "><subfield code="a">picked value</subfield></datafield>
   */
  def getField(recordAsXml: Elem, sourceField: String, marcField: String): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ "metadata" \ "dc" \ sourceField

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    if (data.nonEmpty) {
      <datafield tag={field} ind1={firstInd} ind2={secInd}>
        <subfield code={subfield}>{data.head.text}</subfield>
      </datafield>
    }
    else {NodeSeq.Empty}
  }

}
