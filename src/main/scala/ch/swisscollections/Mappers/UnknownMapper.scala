/*
 * non-marc-mapper
 * Copyright (C) 2022  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.Mappers

import scala.xml.Elem

class UnknownMapper extends Mapper {
  //this returns an empty record and is the fallback mapper when a mapper for this record cannot be found
  def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      {recordAsXml \\ "header"}
      <metadata>
      </metadata>
    </record>
  }

}
