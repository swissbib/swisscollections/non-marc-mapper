package ch.swisscollections.Mappers.Mods


import scala.xml.{Elem, NodeBuffer, NodeSeq}

class ERaraMapper extends ModsMapper {

  private lazy val sourceSubfield = Map (
    "gnd-content" -> "2",
    "doi" -> "2",
  )
  override def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      {recordAsXml \\ "header"}
      <metadata>
        <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
        <leader>00000nam a2200205uc 4500</leader>
        <controlfield tag="001">ERA{(recordAsXml \ "header" \ "identifier").text.substring(18)}</controlfield>
        {getDatestamp(recordAsXml)}
        <controlfield tag="008">{getProcessDate}{getDate(recordAsXml)}xx ||||| |||||||| ||{if ((recordAsXml \ "metadata" \ "mods" \\ "languageTerm").nonEmpty) (recordAsXml \ "metadata" \ "mods" \\ "languageTerm").head.text else "   "} d</controlfield>
        {getField(recordAsXml, "identifier", "020##a", "type", "isbn")}
        {getField(recordAsXml, "identifier", "0247#a", "type", "doi")}
        <datafield tag="035" ind1=" " ind2=" ">
          <subfield code="a">(ERA){(recordAsXml \ "header" \ "identifier").text.substring(18)}</subfield>
        </datafield>
        {getPhysicalLocation(recordAsXml)}
        {getMainTitle(recordAsXml)}
        {getAltTitle(recordAsXml)}
        {getField(recordAsXml, "edition", "250##a")}
        {getPublicationStatement(recordAsXml)}
        {getField(recordAsXml, "extent", "300##a")}
        <datafield tag="347" ind1=" " ind2=" ">
          <subfield code="a">text</subfield>
          <subfield code="b">pdf</subfield>
        </datafield>
        {getSeries(recordAsXml)}
        {getTableOfContents(recordAsXml)}
        {getAccessCondition(recordAsXml)}
        {getTopics(recordAsXml)}
        {getFields(recordAsXml, "genre", "655#7a", "authority", "gnd-content")}
        <datafield tag="655" ind1=" " ind2="7">
          <subfield code="a">Buch/Heft</subfield>
          <subfield code="2">bgs-genre</subfield>
        </datafield>
        {getAuthors(recordAsXml)}
        {getHost(recordAsXml)}
        <datafield tag="856" ind1="4" ind2="0">
          <subfield code="u">https://doi.org/{(recordAsXml \ "metadata" \ "mods" \\ "identifier").filter(node => (node \@ "type") == "doi").text}</subfield>
          <subfield code="3">online</subfield>
        </datafield>
        <datafield tag="856" ind1="4" ind2="0">
          <subfield code="u">https://www.e-rara.ch/download/pdf/{(recordAsXml \ "header" \ "identifier").text.substring(18)}.pdf</subfield>
          <subfield code="3">pdf</subfield>
        </datafield>
        <datafield tag="859" ind1=" " ind2=" ">
          <subfield code="a">era</subfield>
          <subfield code="b">era-{if ((recordAsXml \ "header" \ "setSpec").text.contains("stp")) "stp" else if ((recordAsXml \ "header" \ "setSpec").text.contains("supsi")) "supsi" else ""}</subfield>
          <subfield code="u">{if ((recordAsXml \ "header" \ "setSpec").text.contains("stp")) "https://www.e-rara.ch/stp" else if ((recordAsXml \ "header" \ "setSpec").text.contains("supsi")) "https://www.e-rara.ch/supsi" else ""}</subfield>
        </datafield>
        </marc:record>
      </metadata>
    </record>  }


def getMainTitle(recordAsXml: Elem): NodeSeq = {
  val titleInfos = recordAsXml \ "metadata" \ "mods" \ "titleInfo"
  val mainTitle = titleInfos.filter(_.attributes.isEmpty)

  if (mainTitle.nonEmpty) {
    getTitle(mainTitle, "245")
  }
  else NodeSeq.Empty
}

  def getAltTitle(recordAsXml: Elem): NodeSeq = {
    val titleInfos = recordAsXml \ "metadata" \ "mods" \ "titleInfo"
    val altTitle = titleInfos.filter(_ \@ "type" == "alternative")

    if (altTitle.nonEmpty) {
      getTitle(altTitle, "246")
    }
    else NodeSeq.Empty
  }

  def getPublicationStatement(recordAsXml: Elem): NodeSeq = {
    val place = (recordAsXml \ "metadata" \ "mods" \ "originInfo" \ "place" \ "placeTerm").filter(_ \@ "type" == "text")
    val publisher = recordAsXml \ "metadata" \ "mods" \ "originInfo" \ "publisher"
    val startDate = (recordAsXml \ "metadata" \ "mods" \ "extension" \ "originInfo" \ "dateCreated").filter(_ \@ "point" == "start")
    val endDate = (recordAsXml \ "metadata" \ "mods" \ "extension" \ "originInfo" \ "dateCreated").filter(_ \@ "point" == "end")
    val fallbackDate = recordAsXml \ "metadata" \ "mods" \ "originInfo" \ "dateIssued"

    if (startDate.text.trim.nonEmpty) {
      getPublicationStatmentField(place, publisher, startDate, endDate)
    }
    else {
      getPublicationStatmentField(place, publisher, fallbackDate, endDate)
    }

  }

  def getDate(recordAsXml: Elem): String = {
    val startDate = (recordAsXml \ "metadata" \ "mods" \ "extension" \ "originInfo" \ "dateCreated").filter(_ \@ "point" == "start")
    val endDate = (recordAsXml \ "metadata" \ "mods" \ "extension" \ "originInfo" \ "dateCreated").filter(_ \@ "point" == "end")
    val fallbackDate = recordAsXml \ "metadata" \ "mods" \ "originInfo" \ "dateIssued"

    if (startDate.text.trim.nonEmpty && endDate.text.trim.nonEmpty) "m" + startDate.text + endDate.text
    else if (startDate.text.trim.nonEmpty) "s" + startDate.text + "    "
    else if (fallbackDate.text.trim.nonEmpty) "s" + fallbackDate.text + "    "
    else "u1uuuuuuu"
  }

  def getTableOfContents(recordAsXml: Elem): NodeSeq = {
    val tablesOfContents = recordAsXml \ "metadata" \ "mods" \ "tableOfContents"

    val fields = new NodeBuffer

    if (tablesOfContents.nonEmpty) {
      for (toc <- tablesOfContents) yield {
        fields +=
          <datafield tag="500" ind1=" " ind2=" ">
            <subfield code="a">{toc.text}</subfield>
          </datafield>
      }
    }

    fields
  }

  def getPhysicalLocation(recordAsXml: Elem): NodeSeq = {
    val copies = recordAsXml \ "metadata" \ "mods" \\ "copyInformation"
    val id = recordAsXml \ "metadata" \ "mods" \ "recordInfo" \ "recordIdentifier"

    val fields = new NodeBuffer

    if (copies.nonEmpty) {
      for (copy <- copies) yield {
        fields +=
          <datafield tag="090" ind1=" " ind2=" ">
            <subfield code="a">{(copy \ "subLocation").text}</subfield>
            {if ((copy \ "shelfLocator").nonEmpty) <subfield code="b">{(copy \ "shelfLocator").text}</subfield>}
            <subfield code="c">{id.text}</subfield>
          </datafield>
      }
    }

    fields
  }

  def getAccessCondition(recordAsXml: Elem): NodeSeq = {
    val accessCondition = (recordAsXml \\ "accessCondition" \ "@displayLabel").text
    val accessConditionUrl = (recordAsXml \\ "accessCondition" \ "@{http://www.w3.org/1999/xlink}href").text

    <datafield tag="540" ind1=" " ind2=" ">
      <subfield code="a">{accessCondition}</subfield>
      <subfield code="u">{accessConditionUrl}</subfield>
    </datafield>
  }

  def getHost(recordAsXml: Elem): NodeSeq = {
    val hostTitle = (recordAsXml \ "metadata" \ "mods" \ "relatedItem").filter(_ \@ "type" == "host")
    val title = hostTitle \ "titleInfo"
    val hostPart = (recordAsXml \ "metadata" \ "mods" \ "part").filter(_ \@ "type" == "host")
    val part = hostPart \ "text"

    if (title.nonEmpty) {
      <datafield tag="773" ind1=" " ind2=" ">
        <subfield code="t">{title.text}</subfield>
        {if (part.nonEmpty) <subfield code="g">{part.text}</subfield>}
      </datafield>
    }

    else NodeSeq.Empty

  }

  def getSeries(recordAsXml: Elem): NodeSeq = {
    val series = (recordAsXml \ "metadata" \ "mods" \ "relatedItem").filter(_ \@ "type" == "series")

    val fields = new NodeBuffer

    if (series.nonEmpty) {
      for (serie <- series) yield {
        val title = serie \ "titleInfo" \ "title"
        val number = serie \ "part" \ "detail" \ "number"
        fields +=
          <datafield tag="490">
            <subfield code="a">{title.text}</subfield>
            {if (number.nonEmpty) <subfield code="v">{number.text}</subfield>}
          </datafield>
      }
    }

    fields
  }

  def getTopics(recordAsXml: Elem): NodeSeq = {
    val subjects = recordAsXml \ "metadata" \ "mods" \ "subject"

    val fields = new NodeBuffer

    if (subjects.nonEmpty) {
      for (subject <- subjects) yield {
        val authority = subject \@ "authority"
        val topic = subject \ "topic"
        val geographic = subject \ "geographic"
        val gndId = (subject \@ "valueURI").replace("http://d-nb.info/gnd/", "(DE-588)")

        if (topic.nonEmpty) {
          if (List("gnd", "ethudk", "jurivoc", "sbt12", "rero", "swd").contains(authority)) {
            fields +=
              <datafield tag="650" ind1=" " ind2="7">
                <subfield code="a">{topic.map(_.text).mkString(", ")}</subfield>
                {if (geographic.nonEmpty) <subfield code="z">{geographic.map(_.text).mkString(", ")}</subfield>}
                {if (gndId.nonEmpty) <subfield code="0">{gndId}</subfield>}
                <subfield code="2">{authority}</subfield>
              </datafield>
          }
          else if (authority == "lcsh") {
            fields +=
              <datafield tag="650" ind1=" " ind2="0">
                <subfield code="a">{topic.map(_.text).mkString(", ")}</subfield>
                {if (geographic.nonEmpty) <subfield code="z">{geographic.map(_.text).mkString(", ")}</subfield>}
              </datafield>
          }
          else {
            fields +=
              <datafield tag="650" ind1=" " ind2="4">
                <subfield code="a">{topic.map(_.text).mkString(", ")}</subfield>
                {if (geographic.nonEmpty) <subfield code="z">{geographic.map(_.text).mkString(", ")}</subfield>}
              </datafield>
          }
        }

      }
    }

    fields
  }

  def getAuthors(recordAsXml: Elem): NodeSeq = {
    val persons = (recordAsXml \ "metadata" \ "mods" \ "name").filter(_ \@ "type" == "personal")
    val corporates = (recordAsXml \ "metadata" \ "mods" \ "name").filter(_ \@ "type" == "corporate")
    val conferences = (recordAsXml \ "metadata" \ "mods" \ "name").filter(_ \@ "type" == "conference")

    getAuthorFields(persons, corporates, conferences)
  }



  /**
   * Creates a marc field based on a record and a source Field
   * @param recordAsXml the source record
   * @param sourceField the name of the xml element to pick (for example ScopeAndContent)
   * @param marcField the name of the marc Field to create, this could contains indicators and subfields (for example 500##a)
   * @return an xml node, for example <datafield tag="500" ind1=" " ind2=" "><subfield code="a">picked value</subfield></datafield>
   */
  def getField(recordAsXml: Elem, sourceField: String, marcField: String, attribute: String = "", attributeValue: String = ""): NodeSeq = {
    val data: NodeSeq  = if (attribute.nonEmpty && attributeValue.nonEmpty) {
      (recordAsXml \ "metadata" \ "mods" \\ sourceField).filter(node => (node \@ attribute) == attributeValue)
    } else {
      recordAsXml \ "metadata" \ "mods" \\ sourceField
    }

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    if (data.nonEmpty) {
      <datafield tag={field} ind1={firstInd} ind2={secInd}>
        <subfield code={subfield}>{data.head.text}</subfield>
        {if (sourceSubfield.contains(attributeValue)) <subfield code={sourceSubfield(attributeValue)}>{attributeValue}</subfield>}
      </datafield>
    }
    else {NodeSeq.Empty}
  }

  def getFields(recordAsXml: Elem, sourceField: String, marcField: String, attribute: String = "", attributeValue: String = ""): NodeSeq = {
    val data: NodeSeq  = if (attribute.nonEmpty && attributeValue.nonEmpty) {
      (recordAsXml \ "metadata" \ "mods" \\ sourceField).filter(node => (node \@ attribute) == attributeValue)
    } else {
      recordAsXml \ "metadata" \ "mods" \\ sourceField
    }

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    val fields = new NodeBuffer

    if (data.nonEmpty) {
      for (content <- data) yield {
        fields +=
          <datafield tag={field} ind1={firstInd} ind2={secInd}>
            <subfield code={subfield}>{content.text}</subfield>
            {if (sourceSubfield.contains(attributeValue)) <subfield code={sourceSubfield(attributeValue)}>{attributeValue}</subfield>}
          </datafield>
      }
    }
    fields
  }
}
