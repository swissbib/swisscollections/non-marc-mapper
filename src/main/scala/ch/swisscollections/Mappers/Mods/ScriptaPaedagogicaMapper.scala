/*
 * non-marc-mapper
 * Copyright (C) 2022  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.Mappers.Mods

import scala.language.postfixOps
import scala.xml.NodeSeq.fromSeq
import scala.xml.{Elem, Node, NodeBuffer, NodeSeq, XML}

class ScriptaPaedagogicaMapper extends ModsMapper {

  private lazy val licenceUrl = Map (
    "Public Domain Mark 1.0" -> "https://creativecommons.org/publicdomain/zero/1.0/",
    "Urheberrechtsschutz 1.0" -> "http://rightsstatements.org/vocab/InC/1.0/",
    "" -> "",
  )

  override def mapToXmlMarc(recordAsXml: Elem): Elem = {
    val dmdId = (recordAsXml \ "header" \ "dmdId").text
    val childDiv = (recordAsXml \ "metadata" \ "structMap" \\ "div").filter(_ \@ "DMDID" == dmdId)
    val childLogId = if (childDiv.nonEmpty) childDiv.head \@ "ID" else ""
    val childType = if (childDiv.nonEmpty) childDiv.head \@ "TYPE" else ""
    val childDmd = (recordAsXml \ "metadata" \ "dmdSec").filter(_ \@ "ID" == dmdId)
    val parentIds = getParents(recordAsXml, dmdId)
    val topDmd = {
      if (parentIds.nonEmpty)(recordAsXml \ "metadata" \ "dmdSec").filter(_ \@ "ID" == parentIds.head)
      else childDmd
    }
    val middleDmd = {
      if (parentIds.length == 2) (recordAsXml \ "metadata" \ "dmdSec").filter(_ \@ "ID" == parentIds(1))
      else NodeSeq.Empty
    }
    val superDiv = (recordAsXml \ "metadata" \ "structMap" \ "div").headOption.getOrElse(NodeSeq.Empty)
    val superTitle: Option[String] = superDiv \@ "DMDID" match {
      case "" => superDiv \@ "LABEL" match {
        case "" => None
        case label => Some(label)
      }
      case _ => None // DMDID exists, so we don't set superTitle
    }

    <record>
      {recordAsXml \\ "header" \ "identifier"}
      <metadata>
        <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
          <leader>00000na{if (parentIds.nonEmpty) "a" else "m"} a2200205uc 4500</leader>
          <controlfield tag="001">BBF{(recordAsXml \ "header" \ "identifier").text}</controlfield>
          {getDatestamp(recordAsXml)}
          <controlfield tag="008">{getProcessDate}{getDate(topDmd)}xx ||||| |||||||| ||{getLanguage(childDmd, topDmd)} d</controlfield>
          <datafield tag="035" ind1=" " ind2=" ">
            <subfield code="a">(BBF){(recordAsXml \ "header" \ "identifier").text}</subfield>
          </datafield>
          {getLanguages(childDmd, topDmd)}
          {getPhysicalLocation(topDmd)}
          {getMainTitle(childDmd)}
          {getPublicationStatement(topDmd)}
          {if (parentIds.isEmpty) getExtent(topDmd)}
          <datafield tag="347" ind1=" " ind2=" ">
            <subfield code="a">text</subfield>
            <subfield code="b">pdf</subfield>
          </datafield>
          {if (parentIds.isEmpty && superTitle.nonEmpty)
          <datafield tag="490" ind1=" " ind2=" ">
            <subfield code="a">{superTitle.getOrElse("")}</subfield>
            {if ((topDmd \\ "part" \\ "number").nonEmpty)
            <subfield code="v">{(topDmd \\ "part" \\ "number").text}</subfield>
            }
          </datafield>
          }
          {if (parentIds.isEmpty) getContent(recordAsXml)}
          {getAccessCondition(topDmd)}
          {getPersonSubjects(childDmd)}
          {getTexttype(topDmd)}
          {getAuthors(childDmd)}
          {if (parentIds.nonEmpty) getAuthors(topDmd)}
          {if (parentIds.nonEmpty) {
          if (childType != "Article") getHost(topDmd, middleDmd, superTitle)
          else getHost(topDmd, middleDmd)
        }
          }
          {getLinks(topDmd, childLogId, childType, recordAsXml)}
          {getSource(topDmd)}
        </marc:record>
      </metadata>
    </record>
  }

  def getCollection(classification: String): String = {
    if (classification.toLowerCase.contains("kinderbuecher")) "kind"
    else if (classification.toLowerCase.contains("paedagogische schriften")) "mon"
    else if (classification.toLowerCase.contains("paedagogische nachschlagewerke")) "lex"
    else if (classification.toLowerCase.contains("paedagogische zeitschriften")) "zs"
    else if (classification.toLowerCase.contains("paedagogischezeitschriften")) "zs"
    else ""
  }

  def getSet(classification: String): String = {
    if (classification.contains("16. Jahrhundert")) "16jhr"
    else if (classification.contains("17. Jahrhundert")) "17jhr"
    else if (classification.contains("18. Jahrhundert")) "18jhr"
    else if (classification.contains("19. Jahrhundert")) "19jhr"
    else if (classification.contains("20. Jahrhundert")) "20jhr"
    else if (classification.contains("21. Jahrhundert")) "21jhr"
    else ""
  }

  def getSource(topDmd: NodeSeq): NodeSeq = {
    val classifications = topDmd \\ "classification"

    val fields = new NodeBuffer

    if (classifications.nonEmpty) {
      for (classification <- classifications) yield {
        val collection = getCollection(classification.text)
        fields +=
          <datafield tag="859" ind1=" " ind2=" ">
            <subfield code="a">bbf</subfield>
            <subfield code="b">bbf-spo-{collection}</subfield>
            {if (collection == "mon")
            <subfield code="c">bbf-spo-{collection}-{getSet(classification.text)}</subfield>
            }
            <subfield code="u">https://scripta.bbf.dipf.de/</subfield>
          </datafield>
      }
    }
    val uniqueFieldsSet = fields.map(_.toString()).toSet.map(XML.loadString)
    val uniqueFields: NodeSeq = NodeSeq.fromSeq(uniqueFieldsSet.toSeq)
    uniqueFields
  }

  def getTexttype(topDmd: NodeSeq): NodeSeq = {
    val classifications = topDmd \\ "classification"

    val fields = new NodeBuffer

    if (classifications.nonEmpty) {
      for (classification <- classifications) yield {
        val texttype = {
          if (classification.text.toLowerCase.contains("kinderbuecher")) "Buch/Heft"
          else if (classification.text.toLowerCase.contains("paedagogische schriften")) "Buch/Heft"
          else if (classification.text.toLowerCase.contains("paedagogische nachschlagewerke")) "Artikel (Lexikon)"
          else if (classification.text.toLowerCase.contains("paedagogische zeitschriften")) "Artikel (Zeitschrift/Periodikum)"
          else if (classification.text.toLowerCase.contains("paedagogischezeitschriften")) "Artikel (Zeitschrift/Periodikum)"
          else ""
        }
        fields +=
          <datafield tag="655" ind1=" " ind2="7">
            <subfield code="a">{texttype}</subfield>
            <subfield code="2">bgs-genre</subfield>
          </datafield>
      }
    }
    val uniqueFieldsSet = fields.map(_.toString()).toSet.map(XML.loadString)
    val uniqueFields: NodeSeq = NodeSeq.fromSeq(uniqueFieldsSet.toSeq)
    uniqueFields
  }

  def getDate(dmdSec: NodeSeq): String = {
    val date = (dmdSec \\ "originInfo" \ "dateIssued").text.trim

    if (date.nonEmpty && date.length == 4 && date.matches("[0-9]+")) "s" + date + "    "
    else "u1uuuuuuu"
  }

  def getLanguage(childDmd: NodeSeq, topDmd: NodeSeq): String = {
    val childLanguage = (childDmd \\ "language" \ "languageTerm").filter(_ \@ "type" == "code")
    val topLanguage = (topDmd \\ "language" \ "languageTerm").filter(_ \@ "type" == "code")

    if (childLanguage.nonEmpty) childLanguage.head.text
    else if (topLanguage.nonEmpty) topLanguage.head.text
    else "   "
  }

  def getLanguages(childDmd: NodeSeq, topDmd: NodeSeq): NodeSeq = {
    val childLanguages = (childDmd \\ "language" \ "languageTerm").filter(_ \@ "type" == "code")
    val topLanguages = (topDmd \\ "language" \ "languageTerm").filter(_ \@ "type" == "code")

    if (childLanguages.nonEmpty && childLanguages.length > 1) {
      <datafield tag="041" ind1=" " ind2=" ">
        {for (childLanguage <- childLanguages) yield {
        <subfield code="a">{childLanguage.text}</subfield>
      }
        }
      </datafield>
    }

    else if (topLanguages.nonEmpty && topLanguages.length > 1) {
      <datafield tag="041" ind1=" " ind2=" ">
        {for (topLanguage <- topLanguages) yield {
        <subfield code="a">{topLanguage.text}</subfield>
      }
        }
      </datafield>
    }

    else NodeSeq.Empty

  }

  def getPhysicalLocation(dmdSec: NodeSeq): NodeSeq = {
    val locations = dmdSec \\ "location"
    val id = dmdSec \\"mods" \ "recordInfo" \ "recordIdentifier"

    val fields = new NodeBuffer

    if (locations.nonEmpty) {
      for (location <- locations) yield {
        fields +=
          <datafield tag="090" ind1=" " ind2=" ">
            <subfield code="a">{(location \ "physicalLocation").text}</subfield>
            <subfield code="b">{(location \ "shelfLocator").text}</subfield>
            <subfield code="c">{id.text}</subfield>
          </datafield>
      }
    }
    else if (id.nonEmpty) {
      fields +=
        <datafield tag="090" ind1=" " ind2=" ">
          <subfield code="c">{id.text}</subfield>
        </datafield>
    }

    fields
  }

  def getAccessCondition(dmdSec: NodeSeq): NodeSeq = {
    val accessConditions = (dmdSec \\ "accessCondition").filter(_ \@ "type" == "use and reproduction")
    val note = (dmdSec \\ "accessCondition").filter(_ \@ "type" == "out of print work")

    val fields = new NodeBuffer

    if (accessConditions.nonEmpty) {
      for (accessCondition <- accessConditions) yield {
        fields +=
          <datafield tag="540" ind1=" " ind2=" ">
            <subfield code="a">{accessCondition.text}</subfield>
            {if (note.nonEmpty) <subfield code="b">{note.text}</subfield>}
            {if (licenceUrl.withDefaultValue("")(accessCondition.text) != "") <subfield code="u">{licenceUrl.withDefaultValue("")(accessCondition.text)}</subfield>}
          </datafield>
      }
    }
    fields
  }

  def getHost(topDmd: NodeSeq, middleDmd: NodeSeq, superTitle: Option[String] = None): NodeSeq = {
    val titleInfos = topDmd \\ "titleInfo"
    val mainTitle = titleInfos.filter(_.attributes.isEmpty)
    val altTitle = titleInfos.filter(_ \@ "type" == "alternative")
    val part = topDmd \\ "part" \\ "number"
    val middleTitle = middleDmd \\ "titleInfo" \ "title"

    if (mainTitle.nonEmpty) {
      val title = (mainTitle \ "title")
      val subTitle = (mainTitle \ "subTitle")
      val combinedTitle = if (subTitle.nonEmpty) title.text + " : " +  subTitle.text else title.text

      <datafield tag="773" ind1=" " ind2=" ">
        <subfield code="t">{if (superTitle.nonEmpty) superTitle.getOrElse("") + "."}{combinedTitle}{if (altTitle.nonEmpty) " (" + altTitle.text + ")" }</subfield>
        {if (middleTitle.nonEmpty && part.nonEmpty) <subfield code="g">{part.text + ", " + middleTitle.text}</subfield>
      else if (middleTitle.nonEmpty) <subfield code="g">{middleTitle.text}</subfield>}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getContent(recordAsXml: NodeSeq): NodeSeq = {
    val contents = (recordAsXml \ "metadata" \ "structMap" \\ "div").filter(div => {
      val divType = div \@ "TYPE"
      val label = div \@ "LABEL"
      (divType == "Chapter" || divType == "PartOfWork") && !label.startsWith("[")
    })
    val fields = new NodeBuffer

    if (contents.nonEmpty) {
      for (content <- contents) yield
        fields +=
          <datafield tag="505" ind1="0" ind2="0">
            <subfield code="a">{(content \@ "LABEL").replace("¬", "")}</subfield>
          </datafield>
    }
    fields
  }

  def getPublicationStatement(dmdSec: NodeSeq): NodeSeq = {
    val originInfo = (dmdSec \\ "originInfo").filterNot(_ \ "dateCaptured" nonEmpty)
    val place = (originInfo \ "place" \ "placeTerm").filter(_ \@ "type" == "text")
    val publisher = originInfo \ "publisher"
    val startDate = originInfo \ "dateIssued"
    val endDate = NodeSeq.Empty

    getPublicationStatmentField(place, publisher, startDate, endDate)

  }

  def getExtent(dmdSec: NodeSeq): NodeSeq = {
    val extent = dmdSec \\ "physicalDescription" \ "extent"

    if (extent.nonEmpty) {
      <datafield tag="300" ind1=" " ind2=" ">
        <subfield code="a">{extent.head.text}</subfield>
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getMainTitle(dmdSec: NodeSeq): NodeSeq = {
    val titleInfos = dmdSec \\ "titleInfo"
    val mainTitle = titleInfos.filter(_.attributes.isEmpty)

    if (mainTitle.nonEmpty) {
      getTitle(mainTitle, "245")
    }
    else NodeSeq.Empty
  }

  def getAuthors(dmdSec: NodeSeq): NodeSeq = {
    val persons = (dmdSec \\ "mods" \ "name").filter(_ \@ "type" == "personal")
    val corporates = (dmdSec \\ "mods" \ "name").filter(_ \@ "type" == "corporate")
    val conferences = (dmdSec \\ "mods" \ "name").filter(_ \@ "type" == "conference")

    getAuthorFields(persons, corporates, conferences)
  }

  def getPersonSubjects(dmdSec: NodeSeq): NodeSeq = {
    val persons = (dmdSec \\ "mods" \ "subject" \ "name").filter(_ \@ "type" == "personal")
    val fields = new NodeBuffer

    if (persons.nonEmpty) {
      for (person <- persons) yield {
        val name = person \ "displayForm"
        val identifier = person \ "@valueURI"
        val date = (person \ "namePart").filter(_ \@ "type" == "date")
        fields +=
          <datafield tag="600" ind1=" " ind2="4">
            <subfield code="a">{name.text}</subfield>
            {if (date.text.nonEmpty) <subfield code="d">{date.text}</subfield>}
            {if (identifier.text.nonEmpty && identifier.text.startsWith("http://d-nb.info/gnd/")) <subfield code="0">(DE-588){identifier.text.substring(21)}</subfield>}
          </datafield>
      }
    }
    fields
  }

  def getLinks(dmdSec: NodeSeq, logId: String, childType: String, recordAsXml: NodeSeq): NodeSeq = {
    val id = (dmdSec \\ "mods" \ "recordInfo" \ "recordIdentifier").head.text
    val fields = new NodeBuffer

    {if (logId.nonEmpty) {
      fields +=
      <datafield tag="856" ind1="4" ind2="0">
        <subfield code="u">https://scripta.bbf.dipf.de/viewer/api/v1/records/{id}/sections/{logId}/pdf</subfield>
        <subfield code="3">pdf</subfield>
      </datafield>
    } else {
      fields +=
      <datafield tag="856" ind1="4" ind2="0">
        <subfield code="u">https://scripta.bbf.dipf.de/viewer/api/v1/records/{id}/pdf</subfield>
        <subfield code="3">pdf</subfield>
      </datafield>
    }
    }
    {if (childType == "Article" || childType == "Lemma") {
      val structMapPhysicalDiv = (recordAsXml \ "metadata" \ "structMapFirstPage" \\ "div")
      val urn = if (structMapPhysicalDiv.nonEmpty) structMapPhysicalDiv.head \@ "CONTENTIDS" else ""
      val physId = if (structMapPhysicalDiv.nonEmpty) structMapPhysicalDiv.head \@ "ID" else ""
      val fileName = if (physId.nonEmpty && physId.length == 9) "0000" + physId.substring(5,9)
      fields +=
      <datafield tag="856" ind1="4" ind2="0">
        <subfield code="u">https://scripta.bbf.dipf.de/viewer/resolver?urn={urn}</subfield>
        <subfield code="3">online</subfield>
      </datafield>
      fields +=
      <datafield tag="856" ind1="4" ind2="8">
        <subfield code="u">https://bbf-scripta-paedagogica.de/viewer/api/v1/records/{id}/files/images/{fileName}.tif</subfield>
        <subfield code="3">thumbnail iiif</subfield>
      </datafield>
    }
    else {
      fields +=
      <datafield tag="856" ind1="4" ind2="0">
        <subfield code="u">https://scripta.bbf.dipf.de/viewer/image/{id}/</subfield>
        <subfield code="3">online</subfield>
      </datafield>
      fields +=
      <datafield tag="856" ind1="4" ind2="8">
        <subfield code="u">https://bbf-scripta-paedagogica.de/viewer/api/v1/records/{id}/files/images/00000001.tif</subfield>
        <subfield code="3">thumbnail iiif</subfield>
      </datafield>
    }}
    fields
  }

  /**
   * Split a single METS record into multiple records
   * @param kafka_message_key
   * @param record
   * @return
   */
  def splitRecord(kafka_message_key: String, record: Elem): Seq[(String, String)] = {
    val dmdIds = getListofRelevantDmdId(record)

    println(s"$kafka_message_key splitted in ${dmdIds.size} record(s)")

    val records = dmdIds.map { dmdId =>
      val individualRecord = createIndividualRecord(record, dmdId)
      val identifier = s"(BBF)${(individualRecord \ "header" \ "identifier").text}"
      val recordString = individualRecord.toString()
      (identifier, recordString)
    }
    records
  }

  /**
   * Get the different parts of this METS record which will create an individual record in solr
   * @param recordAsXml the METS record
   * @return
   */
  def getListofRelevantDmdId(recordAsXml: Elem): Seq[String] = {
    val acceptedTypes = List(
      "Article",
      "Lemma"
    )

    val structMapLogical = getStructMapLogical(recordAsXml)

    val dmdIds = (structMapLogical \\ "div")
      .filter(div => acceptedTypes.contains(div \@ "TYPE"))
      .map(div => div \@ "DMDID")
    dmdIds.size match {
      case size if size > 0 => dmdIds
      case _ => List(getFirstDmdSecId(recordAsXml))
    }
  }


  /**
   * Get the first dmdsection id
   * @param recordAsXml
   * @return
   */
  def getFirstDmdSecId(recordAsXml: Elem): String = {
    (recordAsXml \ "metadata" \ "mets" \ "dmdSec").head \@ "ID"
  }

  /**
   * Get the logical structmap of this record
   * @param recordAsXml
   * @return
   */
  def getStructMapLogical(recordAsXml: Elem): Node = {
    (recordAsXml \\ "structMap").find(_.attribute("TYPE").exists(_.text == "LOGICAL")).head
  }

  def getStructMapPhysical(recordAsXml: Elem, dmdId: String): Node = {

    val dmdStructMapLogical = (recordAsXml \ "metadata" \\ "structMap" \\ "div").filter(_ \@ "DMDID" == dmdId)
    val logId = if (dmdStructMapLogical.nonEmpty) dmdStructMapLogical.head \@ "ID" else ""
      // <mets:smLink xlink:to="PHYS_0001" xlink:from="LOG_0003" />
    val logStructLink = (recordAsXml \ "metadata" \\ "structLink" \\ "smLink").filter(_ \@ "{http://www.w3.org/1999/xlink}from" == logId)
    val physId = if (logStructLink.nonEmpty) logStructLink.head \@ "{http://www.w3.org/1999/xlink}to" else ""
    val physStructMapPhysical = (recordAsXml \ "metadata" \\ "structMap" \\ "div").filter(_\@ "ID" == physId)

    <mets:structMapFirstPage TYPE="PHYSICAL" xmlns:mets="http://www.loc.gov/METS/" xmlns:dv="http://dfg-viewer.de/" xmlns:mods="http://www.loc.gov/mods/v3" xmlns:xlink="http://www.w3.org/1999/xlink">
    {physStructMapPhysical}
    </mets:structMapFirstPage>
  }

  /**
   * Get a List of the DMD identifiers (DMDID) of all the parents in the structmap logical of the dmdId
   * @param recordAsXml
   * @param dmdId
   * @return
   */
  def getParents(recordAsXml: Elem, dmdId: String): Seq[String] = {
    val structMapLogical = getStructMapLogical(recordAsXml)
    for {
      div <- structMapLogical \\ "div"
      if div\@ "DMDID" != ""
      if div\@ "DMDID" != dmdId
      if isParentOf(div, dmdId)
    } yield div \@ "DMDID"
  }

  /**
   * If there is a node with dmdId as its DMDID attribute value, return Some(Node)
   * Otherwise return None
   * @param someXml the source xml (often used only for the structmap)
   * @param dmdId the id to search for
   * @return
   */
  def getNodeBasedOnDmdid(someXml: Node, dmdId: String): Option[Node] = {
    (someXml \\ "div").find(_.attribute("DMDID").exists(_.text == dmdId))
  }

  /**
   * Check if the xml node currentNode has a child with the dmdId value in the DMDID attribute
   * @param parentNode The potential parent to check
   * @param dmdId the dmdid to check, is it part of currentNode ?
   * @return
   */
  def isParentOf(parentNode: Node, dmdId: String): Boolean = {
    val child = getNodeBasedOnDmdid(parentNode, dmdId)
    child match {
      case Some(_) => true
      case None => false
    }
  }

  def createIndividualRecord(recordAsXml: Elem, dmdId: String): Elem = {
    val parentDmdIds = getParents(recordAsXml, dmdId)
    //dmdId hast the form DMDLOG_0029, we take what is after the _
    val newIdSuffix = dmdId match {
      case s if s.startsWith("DMDLOG_") => s.substring(7)
      case _ => dmdId
    }

    //id for the whole mets structure, usually something like 025763679_0001
    val oldId = (recordAsXml \ "header" \ "identifier").text

    //val oldIdPrefix = oldId.lastIndexOf("_") match {
    //  case -1 => oldId //if the oldId doesn't have a _
    //  case _ => oldId.substring(0, oldId.lastIndexOf("_"))
    //}


    val res =
      <record>
        <header>
          <identifier>{oldId}_{newIdSuffix}</identifier>
          <dmdId>{dmdId}</dmdId>
        </header>
        <metadata>
          <mets:mets xmlns:mets="http://www.loc.gov/METS/" xmlns:dv="http://dfg-viewer.de/" xmlns:mods="http://www.loc.gov/mods/v3" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd http://www.loc.gov/METS/ http://www.loc.gov/standards/mets/version17/mets.v1-7.xsd" OBJID="2730">
          </mets:mets>
          { recordAsXml \ "metadata" \ "mets" \ "metsHdr" }
          { getDmdSections(recordAsXml, parentDmdIds :+ dmdId)}
          { getStructMapLogical(recordAsXml)}
          { getStructMapPhysical(recordAsXml, dmdId)}
        </metadata>
    </record>
    res
  }

  def getDmdSections(recordAsXml: Elem, dmdIds: Seq[String]): Seq[Node] = {
    val res = for {
      dmdSec <- recordAsXml \ "metadata" \ "mets" \ "dmdSec"
      if dmdIds.contains(dmdSec \@ "ID")
    } yield dmdSec
    res
  }

}
