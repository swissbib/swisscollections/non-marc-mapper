/*
 * Non-marc Mapper
 * Copyright (C) 2024  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.swisscollections.Mappers.Mods

import ch.swisscollections.Mappers.Mapper

import scala.xml.{Elem, NodeBuffer, NodeSeq}

class ModsMapper extends Mapper {
  def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      {recordAsXml \\ "header"}
      <metadata>
      </metadata>
    </record>
  }

  def getTitle(titleInfo: NodeSeq, field: String): NodeSeq = {
    val title = titleInfo \ "title"
    val subtitle = titleInfo \ "subTitle"
    val partName = titleInfo \ "partName"
    val partNumber = titleInfo \ "partNumber"
    val article = titleInfo \ "nonSort"

    if (title.nonEmpty) {
      <datafield tag={field} ind1=" " ind2=" ">
        <subfield code="a">{if (article.nonEmpty) "<<" + article.text.trim + " >>" + title.text else title.text}</subfield>
        {if (subtitle.nonEmpty) <subfield code="b">{subtitle.text}</subfield>}
        {if (partNumber.nonEmpty) <subfield code="n">{partNumber.text}</subfield>}
        {if (partName.nonEmpty) <subfield code="p">{partName.text}</subfield>}
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getPublicationStatmentField(place: NodeSeq, publisher: NodeSeq, startDate: NodeSeq, endDate: NodeSeq): NodeSeq = {
    if (place.nonEmpty || publisher.nonEmpty || startDate.nonEmpty) {
      <datafield tag="264" ind1=" " ind2="1">
        {if (place.nonEmpty) <subfield code="a">{place.map(_.text).mkString(", ")}</subfield>}
        {if (publisher.nonEmpty) <subfield code="b">{publisher.map(_.text).mkString(", ")}</subfield>}
        {if (startDate.text.trim.nonEmpty && endDate.text.trim.nonEmpty) <subfield code="c">{startDate.text}-{endDate.text}</subfield>
      else if (startDate.text.trim.nonEmpty)  <subfield code="c">{startDate.text}</subfield>
        }
      </datafield>
    }
    else NodeSeq.Empty
  }

  def getAuthorFields(persons: NodeSeq, corporates: NodeSeq, conferences: NodeSeq): NodeSeq = {
    val fields = new NodeBuffer

    if (persons.nonEmpty) {
      for (person <- persons) yield {
        val name = person \ "displayForm"
        val identifier = person \ "@valueURI"
        val role = (person \\ "roleTerm").filter(_ \@ "type" == "code")
        val date = (person \ "namePart").filter(_ \@ "type" == "date")

        fields +=
          <datafield tag="700" ind1=" " ind2=" ">
            <subfield code="a">{name.text}</subfield>
            {if (date.text.nonEmpty) <subfield code="d">{date.text}</subfield>}
            {if (identifier.text.nonEmpty && identifier.text.startsWith("http://d-nb.info/gnd/")) <subfield code="0">(DE-588){identifier.text.substring(21)}</subfield>}
            {if (role.text.nonEmpty) <subfield code="4">{role.text}</subfield>}
          </datafield>
      }
    }

    if (corporates.nonEmpty) {
      for (corporate <- corporates) yield {
        val name = corporate \ "namePart"
        val identifier = corporate \ "@valueURI"
        val role = (corporate \\ "roleTerm").filter(_ \@ "type" == "code")

        fields +=
          <datafield tag="710" ind1=" " ind2=" ">
            <subfield code="a">{name.map(_.text).mkString(", ")}</subfield>
            {if (identifier.text.nonEmpty && identifier.text.startsWith("http://d-nb.info/gnd/")) <subfield code="0">(DE-588){identifier.text.substring(21)}</subfield>}
            {if (role.text.nonEmpty) <subfield code="4">{role.text}</subfield>}
          </datafield>
      }
    }

    if (conferences.nonEmpty) {
      for (conference <- conferences) yield {
        val name = conference \ "namePart"
        val identifier = conference \ "@valueURI"
        val role = (conference \\ "roleTerm").filter(_ \@ "type" == "code")

        fields +=
          <datafield tag="711" ind1=" " ind2=" ">
            <subfield code="a">{name.map(_.text).mkString(", ")}</subfield>
            {if (identifier.text.nonEmpty && identifier.text.startsWith("http://d-nb.info/gnd/")) <subfield code="0">(DE-588){identifier.text.substring(21)}</subfield>}
            {if (role.text.nonEmpty) <subfield code="4">{role.text}</subfield>}
          </datafield>
      }
    }

    fields
  }
}
