package ch.swisscollections.Mappers

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.xml.{Elem, NodeSeq}

// This is the abstract Mapper which is the base class for all mappers
abstract class Mapper {
  /**
   * This method maps the xml from a source to a marc21 xml which fits swisscollections requirements
   * @param recordAsXml the source record
   * @return
   */
  def mapToXmlMarc(recordAsXml: Elem): Elem

  def getDatestamp(recordAsXml: Elem): NodeSeq = {
    val datestamp = recordAsXml \ "header" \ "datestamp"

    if (datestamp.nonEmpty) {
      val datestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'")
      val dateFormat = new SimpleDateFormat("yyyyMMddhhmmss'.0'")
      <controlfield tag="005">{dateFormat.format(datestampFormat.parse(datestamp.text))}</controlfield>
    }
    else NodeSeq.Empty
  }

  def getProcessDate: String = {
    val format = new SimpleDateFormat("yyMMdd")
    format.format(Calendar.getInstance().getTime)
  }

}
