/*
 * Non-marc Mapper
 * Copyright (C) 2024  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.swisscollections.Mappers

import scala.xml.{Elem, NodeBuffer, NodeSeq}

class BizMapper extends Mapper {

  private lazy val collection = Map(
    "Zeitreihe/Statistik" -> "biz-stat",
    "Datensatz" -> "biz-stat",
    "Dokumentation" -> "biz-doku",
    "Kommentar" -> "biz-comm",
    "Datenanhang" -> "biz-comm",
    "Chronik" -> "biz-chron",
    "Grafik" -> "biz-chron",
    "Kurzbiografie" -> "biz-erz"
  )

  def mapToXmlMarc(recordAsXml: Elem): Elem = {
    <record>
      <metadata>
        <marc:record xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
          <leader>00000nam a2200205uc 4500</leader>
          <controlfield tag="001">BIZ{(recordAsXml \ "Dokument-ID").text}</controlfield>
          <controlfield tag="008">{getProcessDate}{getDate(recordAsXml)}sz ||||| |||||||| ||{(recordAsXml \ "Sprache").text.substring(0,3)} d</controlfield>
          <datafield tag="035" ind1=" " ind2=" ">
            <subfield code="a">(BIZ){(recordAsXml \ "Dokument-ID").text}</subfield>
          </datafield>
          {getLanguage(recordAsXml)}
          {getField(recordAsXml, "Dokument-ID", "090##b")}
          {getField(recordAsXml, "Titel", "245##a")}
          {getField(recordAsXml, "Publikationsdatum", "264#1c")}
          {getField(recordAsXml, "Version", "250##a")}
          <datafield tag="347" ind1=" " ind2=" ">
            <subfield code="a">text</subfield>
            <subfield code="b">{if ((recordAsXml \ "Medienformat").text.contains("csv")) "csv" else "pdf"}</subfield>
          </datafield>
          {getNoteField(recordAsXml, "Zeitraum")}
          {getNoteField(recordAsXml, "Variable_Gliederung")}
          {getNoteField(recordAsXml, "Aggregationsgrad")}
          {getNoteField(recordAsXml, "Bildungstyp")}
          {getNoteField(recordAsXml, "Kommentar")}
          <datafield tag="540" ind1=" " ind2=" ">
            <subfield code="a">Creative Commons CC BY 4.0 International</subfield>
            <subfield code="u">https://creativecommons.org/licenses/by/4.0</subfield>
          </datafield>
          {getSplitField(recordAsXml, "Quelle", "650#4a")}
          {getSplitField(recordAsXml, "Zaehleinheit", "650#4a")}
          {getSplitField(recordAsXml, "Bildungsbereich", "650#4a")}
          {getSplitField(recordAsXml, "Institution", "650#4a")}
          {getSplitField(recordAsXml, "Bildungsraum", "651#4a")}
          <datafield tag="655" ind1=" " ind2="7">
            <subfield code="a">Forschungsdaten</subfield>
            <subfield code="2">bgs-genre</subfield>
          </datafield>
          {if (collection((recordAsXml \ "Dokumenttyp").text) == "biz-stat")
            <datafield tag="655" ind1=" " ind2="7">
              <subfield code="a">Statistik</subfield>
              <subfield code="2">gnd-content</subfield>
            </datafield>
          }
          {getSplitField(recordAsXml, "Ersteller", "700##a")}
          {getField(recordAsXml, "Herausgeber", "710##a", "4", "edt")}
          <datafield tag="856" ind1="4" ind2="8">
            <subfield code="u">{(recordAsXml \ "Downloadlink").text}</subfield>
            <subfield code="3">{if ((recordAsXml \ "Downloadlink").text.contains("csv/file")) "csv" else "pdf"}</subfield>
          </datafield>
          <datafield tag="856" ind1="4" ind2="0">
            <subfield code="u">{(recordAsXml \ "Viewerlink").text}</subfield>
            <subfield code="3">online</subfield>
          </datafield>
          <datafield tag="859" ind1=" " ind2=" ">
            <subfield code="a">biz</subfield>
            <subfield code="b">{collection((recordAsXml \ "Dokumenttyp").text)}</subfield>
            <subfield code="u">https://app.dasch.swiss/project/w0ViRJuOQwy6az9amUg-gw</subfield>
          </datafield>
        </marc:record>
      </metadata>
    </record>
  }

  def getDate(recordAsXml: Elem): String = {
    val date = recordAsXml \ "Publikationsdatum"
    val year = date.text.trim.substring(6,10)
    val month = date.text.trim.substring(3,5)
    val day = date.text.trim.substring(0,2)

    s"e$year$month$day"
  }

  def getLanguage(recordAsXml: Elem): NodeSeq = {
    val languages = (recordAsXml \ "Sprache").text.split(";")
    val field = new NodeBuffer

    if (languages.length > 1) {
      field +=
        <datafield tag="041" ind1=" " ind2=" ">
          {for (language <- languages) yield {
          <subfield code="a">{language.trim}</subfield>
        }}
        </datafield>
    }
    field
  }

  def getNoteField(recordAsXml: Elem, sourceField: String): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ sourceField

    if (data.nonEmpty) {
      <datafield tag="500" ind1=" " ind2=" ">
        <subfield code="a">{sourceField.replace("_", "/")}: {data.head.text}</subfield>
      </datafield>
    }
    else {NodeSeq.Empty}
  }

  def getSplitField(recordAsXml: Elem, sourceField: String, marcField: String): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ sourceField

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    val fields = new NodeBuffer

    if (data.nonEmpty && data.text.contains(";")) {
      for (string <- data.text.split(";")) yield {
        fields +=
          <datafield tag={field} ind1={firstInd} ind2={secInd}>
            <subfield code={subfield}>{string.trim}</subfield>
            </datafield>
      }
    }
    else if (data.nonEmpty) {
      fields +=
        <datafield tag={field} ind1={firstInd} ind2={secInd}>
          <subfield code={subfield}>{data.text}</subfield>
        </datafield>
    }

    fields
  }

  /**
   * Creates a marc field based on a record and a source Field
   * @param recordAsXml the source record
   * @param sourceField the name of the xml element to pick (for example ScopeAndContent)
   * @param marcField the name of the marc Field to create, this could contains indicators and subfields (for example 500##a)
   * @return an xml node, for example <datafield tag="500" ind1=" " ind2=" "><subfield code="a">picked value</subfield></datafield>
   */
  def getField(recordAsXml: Elem, sourceField: String, marcField: String, sourceSubfield: String = "", sourceCode: String = ""): NodeSeq = {
    val data: NodeSeq  = recordAsXml \ sourceField

    val field = marcField.substring(0,3)
    val firstInd = marcField.substring(3,4).replace("#", " ")
    val secInd = marcField.substring(4,5).replace("#", " ")
    val subfield = marcField.substring(5,6)

    if (data.nonEmpty) {
      <datafield tag={field} ind1={firstInd} ind2={secInd}>
        <subfield code={subfield}>{data.head.text}</subfield>
        {if (sourceCode.nonEmpty)
        <subfield code={sourceSubfield}>{sourceCode}</subfield>
        }
      </datafield>
    }
    else {NodeSeq.Empty}
  }

}
