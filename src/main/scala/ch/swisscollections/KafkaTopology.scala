/*
 * non-marc-mapper
 * Copyright (C) 2022  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections


import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.logging.log4j.scala.Logging

class KafkaTopology extends AppSettings with Logging {

  import Serdes._

  def build: Topology = {

    val builder = new StreamsBuilder

    val source = {
      if (inputTopic.contains(",")) {
        //read from multiple input topics
        val topicCollection = inputTopic.split(",").toSet
        builder.stream[String, String](topicCollection)
      } else {
        builder.stream[String, String](inputTopic)
      }

    }

    val nonMarcRecords = source.filter((k,_) => KafkaTopologyUtils.filterRecords(k))

    val Array(deletes, updates) = nonMarcRecords
      .branch(
        (_, v) => KafkaTopologyUtils.isDelete(v),
        (_, _) => true
      )

    updates
      .flatMap( (k,v) => KafkaTopologyUtils.recordSplitter(k,v))
      .mapValues( (k,v) => KafkaTopologyUtils.mapToMarc21(k, v))
      .to(outputTopic)

    deletes
      .map((k,v) => (KafkaTopologyUtils.changeIdForDeletes(k), v))
      .to(deleteTopic)

    builder.build()
  }


}
