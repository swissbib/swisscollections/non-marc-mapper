/*
 * non-marc-mapper
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections


import ch.swisscollections.Mappers.DublinCore.{EPeriodicaMapper, PestalozzianumMapper}
import ch.swisscollections.Mappers.Mods.{ERaraMapper, ScriptaPaedagogicaMapper}
import ch.swisscollections.Mappers.{BizMapper, CmiMapper, EPeriodicaCsvMapper, Mapper, UnknownMapper}
import org.apache.logging.log4j.scala.Logging

import scala.xml.{Elem, Node, NodeBuffer, NodeSeq, XML}
import java.text.SimpleDateFormat
import java.util.Calendar
import scala.collection.mutable.ListBuffer
import scala.util.matching.Regex


object KafkaTopologyUtils extends Logging {

  /**
   * Check if a record is a non Marc Record
   * For the moment we check the kafka message key and if it doesn't contain
   * oai:alma.41SLSP then this is a non-marc record
   *
   * @param oai identifier like (RZS)oai:alma.41SLSP_RZS:9914240917605505
   * @return
   */
  def filterRecords(identifier: String): Boolean = {
    val journalIds = Set("ass-001", "avo-001", "erz-001", "fri-001", "gfs-001", "iua-001", "jpz-001", "jpz-002", "peb-001", "pes-001", "phi-001", "phi-002", "pka-001", "pka-002", "sbo-001", "sch-001", "sch-002", "scp-001", "ssa-001", "ssa-002", "ssa-003", "ssa-004", "ssa-005", "syn-001", "tpz-001", "zrk-001", "zsk-001", "zvl-001", "aip-001", "aip-002", "aip-003", "ani-001", "ani-002", "bkp-001", "bkp-002", "bkp-003", "bpg-001", "bsb-001", "bsf-001", "bso-001", "bso-002", "cbl-001", "cuv-001", "cuv-002", "cuv-003", "cuv-004", "cuv-005", "cuv-006", "cuv-007", "cuv-008", "edu-001", "esi-001", "jbl-001", "jbl-002", "jgs-001", "jus-001", "jus-002", "jus-003", "pbe-001", "pio-001", "ppr-001", "ppr-002", "scs-001", "scs-002", "scs-003", "sle-001", "slz-001", "slz-002", "slz-003", "vsb-001", "bpe-001", "bpe-002", "mgf-001", "mgf-002", "mgf-003", "mgf-004", "mgf-005", "szb-001", "szb-002", "vsh-001", "vsh-002")

    if (identifier.toLowerCase.contains("oai:alma.41slsp")) {
      return false
    }
    if (identifier.toLowerCase.contains("oai:agora.ch")) {
      if (!journalIds.contains(identifier.toLowerCase.substring(17,24))) {
        return false
      }
    }
    true
  }


  /**
   * Check if a message is a delete based on the header status
   *
   * @param message the full oai record with header
   * @return true/false
   */
  def isDelete(record: String): Boolean = {
    val header = XML.loadString(record) \ "header"
    val status = header \ "@status"
    if (status.toString() == "deleted") {
      true
    } else {
      false
    }
  }

  /**
   * Change the id, tranform it from oai id to solr id
   *
   * @param oai_key the id key from oai (for example (ZBCMI)oai:ZBcollections:dbeb348e8cb446a8a341cd49f35c03da )
   * @return the solr id (for example ZBCdbeb348e8cb446a8a341cd49f35c03da)
   */
  def changeIdForDeletes(oai_key: String): String = {
    //toodo maybe make this more generic (take the id after the last ":", but that might not always work, cf. oai:agora.ch:aip-001:1912:3::4) ?
    oai_key
      .replaceFirst("(ZBCMI)oai:ZBcollections:", "ZBC")
      .replaceFirst("(STP)oai:sammlungen.pestalozzianum.ch:_", "STP")
      .replaceFirst("(BBFKIND)", "BBF")
      .replaceFirst("(BBFPAEDLEX)", "BBF")
      .replaceFirst("(BBFPAEDSCR)", "BBF")
      .replaceFirst("(BBFPAEDZTS)", "BBF")
      .replaceFirst("(EP)oai:agora.ch:", "EP")
      .replaceFirst("(ERASUPSI)oai:www.e-rara.ch:", "ERA")
      .replaceFirst("(ERASTP)oai:www.e-rara.ch:", "ERA")
      .replace(":", "_")
  }

  def mapToMarc21(key: String, record: String): String = {
    val mapper = getMapper(key)
    val recordElement =
      try {
        val recordAsXml = XML.loadString(record)
        mapper.mapToXmlMarc(recordAsXml).toString()
      } catch {
        case e: Throwable =>
          logger.error("Problem with record " + record)
          ""
      }
    recordElement
  }

  /**
   * Get the corresponding mapper based on kafka messasge key
   * The kafka message key has a prefix which is based on the name of the config
   * file in the data ingester https://gitlab.switch.ch/swissbib/classic/dataingestion/-/tree/master/configs/oai
   *
   * @param kafka_message_key
   * @return
   */
  def getMapper(kafka_message_key: String): Mapper = {
    if (kafka_message_key.startsWith("(ZBCMI)")) {
      new CmiMapper
    } else if (kafka_message_key.startsWith("(STP)")) {
      new PestalozzianumMapper
    } else if (kafka_message_key.startsWith("(BBF)")) {
      new ScriptaPaedagogicaMapper
    } else if (kafka_message_key.startsWith("(BBFKIND)")) {
      new ScriptaPaedagogicaMapper
    } else if (kafka_message_key.startsWith("(BBFPAEDLEX)")) {
      new ScriptaPaedagogicaMapper
    } else if (kafka_message_key.startsWith("(BBFPAEDSCR)")) {
      new ScriptaPaedagogicaMapper
    } else if (kafka_message_key.startsWith("(BBFPAEDZTS)")) {
      new ScriptaPaedagogicaMapper
    } else if (kafka_message_key.startsWith("(EP)")) {
      new EPeriodicaMapper
    } else if (kafka_message_key.startsWith("(ERASUPSI)")) {
      new ERaraMapper
    } else if (kafka_message_key.startsWith("(ERASTP)")) {
      new ERaraMapper
    } else if (kafka_message_key.startsWith("(BIZ)")) {
      new BizMapper
    } else if (kafka_message_key.startsWith("(EPCSV)")) {
      new EPeriodicaCsvMapper
    } else {
      new UnknownMapper
    }
  }



  def recordSplitter(kafka_message_key: String, record: String): Seq[(String, String)] = {
    if (kafka_message_key.startsWith("(BBF")) {
      val recordElement =
        try {
          XML.loadString(record)
        } catch {
          case e: Throwable =>
            logger.error("Problem with record " + record)
            //todo : do something better here
            <record></record>
        }
      val scriptaPaedagogicaMapper = new ScriptaPaedagogicaMapper
      scriptaPaedagogicaMapper.splitRecord(kafka_message_key, recordElement)

    } else {
      // In this case we don't split the record
      List((kafka_message_key, record))
    }
  }



}
