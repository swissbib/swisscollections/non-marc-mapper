/*
 * non-marc-mapper
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.Mappers.CmiMapper
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import scala.io.Source
import scala.xml.{Elem, NodeSeq, XML}


class CmiMapperTest extends AnyFunSuite {
  lazy val cmiMapper = new CmiMapper

  def loadRecord(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/ZBcollections/record/$filename")
    val record = file.mkString
    val recordAsXml = XML.loadString(record)
    recordAsXml
  }
  def loadResult(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/ZBcollections/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }


  ignore("first mapping test") {
    val recordAsXml = loadRecord("cmi.xml")
    val mappedRecord = cmiMapper.mapToXmlMarc(recordAsXml)

    //should not do anything, but without this, the test fails
    val mappedRecordParsedAgain = XML.loadString(mappedRecord.toString())

    mappedRecordParsedAgain should equal (loadResult("result.xml")) (after being streamlined[Elem])

  }

  test("no callnumber") {
    val recordAsXml = loadRecord("no-callnumber.xml")
    val mappedRecord = cmiMapper.mapToXmlMarc(recordAsXml)
    println(mappedRecord)


  }

  test ("create field") {
    val recordAsXml = loadRecord("cmi.xml")
    val resultElem = cmiMapper.getField(recordAsXml, "Format", "300##a").toString()
    XML.loadString(resultElem) should equal (loadResult("create-field-300.xml")) (after being streamlined[Elem])
  }

  test ("dates") {
    val recordAsXml = loadRecord("cmi.xml")
    val result = cmiMapper.getDate(recordAsXml)
    assert(result == "q17601801")

    val datestamp = cmiMapper.getDatestamp(recordAsXml)
    assert(datestamp.toString() == "<controlfield tag=\"005\">20230421120325.0</controlfield>")
  }

  test("contributors") {
    //mit mehreren 600, mit 100, mit 1 700
    val recordAsXML2 = loadRecord("person-subject.xml")
    val result2Main = cmiMapper.getMainEntry(recordAsXML2).toString()
    val result2Subject = <dummy>{cmiMapper.getSubjectEntry(recordAsXML2)}</dummy>.toString()
    val result2Added = <dummy>{cmiMapper.getAddedEntry(recordAsXML2)}</dummy>.toString()

    XML.loadString(result2Main) should equal (loadResult("person-subject_main.xml")) (after being streamlined[Elem])
    XML.loadString(result2Subject) should equal (loadResult("person-subject_subject.xml")) (after being streamlined[Elem])
    XML.loadString(result2Added) should equal (loadResult("person-subject_added.xml")) (after being streamlined[Elem])

    //mit mehreren Beziehungskennzeichnungen für 100
    val recordAsXML3 = loadRecord("person-mains.xml")
    val result3Main = cmiMapper.getMainEntry(recordAsXML3).toString()
    val result3Added = cmiMapper.getAddedEntry(recordAsXML3).toString()

    XML.loadString(result3Main) should equal (loadResult("person-mains_main.xml")) (after being streamlined[Elem])
    XML.loadString(result3Added) should equal (loadResult("person-mains_added.xml")) (after being streamlined[Elem])

    //ohne 100, mit 1 600, mit mehreren 700, no GND
    val recordAsXML4 = loadRecord("person-no-main.xml")
    val result4Main: NodeSeq = cmiMapper.getMainEntry(recordAsXML4)
    val result4Subject = cmiMapper.getSubjectEntry(recordAsXML4).toString()
    val result4Added = <dummy>{cmiMapper.getAddedEntry(recordAsXML4)}</dummy>.toString()

    assert(result4Main == NodeSeq.Empty)
    XML.loadString(result4Subject) should equal (loadResult("person-no-main_subject.xml")) (after being streamlined[Elem])
    XML.loadString(result4Added) should equal (loadResult("person-no-main_added.xml")) (after being streamlined[Elem])

  }

  test("series") {
    val recordAsXml = loadRecord("cmi.xml")
    val result = cmiMapper.getParent(recordAsXml, "830").toString()

    XML.loadString(result) should equal (loadResult("series.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("bestand.xml")
    val result2 = cmiMapper.getParent(recordAsXml2, "990").toString()
    XML.loadString(result2) should equal (loadResult("parent-unit.xml")) (after being streamlined[Elem])

    val recordAsXml3 = loadRecord("autograph.xml")
    val result3 = cmiMapper.getParent(recordAsXml3, "830").toString()
    println(result3)

    XML.loadString(result3) should equal(loadResult("series-autograph.xml"))(after being streamlined[Elem])
  }

  test("production notice") {
    val recordAsXml = loadRecord("places.xml")
    val result = cmiMapper.getProductionNotice(recordAsXml).toString()

    XML.loadString(result) should equal (loadResult("productionNotice.xml")) (after being streamlined[Elem])
  }
  test("language") {
    val recordAsXml = loadRecord("languages.xml")
    val languageFirst = (recordAsXml \ "metadata" \ "Item" \ "Languages" \ "Language").head.text
    val result = cmiMapper.getLanguages(recordAsXml).toString()

    assert(languageFirst == "ger")
    XML.loadString(result) should equal (loadResult("language.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("places.xml")
    val result2 = cmiMapper.getLanguages(recordAsXml2)
    assert(result2 == NodeSeq.Empty)
  }

  test("topics") {
    val recordAsXml = loadRecord("cmi.xml")
    val result = <dummy>{cmiMapper.getTopics(recordAsXml)}</dummy>.toString()

    XML.loadString(result) should equal (loadResult("topics.xml")) (after being streamlined[Elem])
  }

  test("formats") {
    val recordAsXml = loadRecord("person-subject.xml")
    val ldr = cmiMapper.getFormat(recordAsXml)
    val matSpec = cmiMapper.getMaterialSpecific(recordAsXml)
    val gndContent = <dummy>{cmiMapper.getContent(recordAsXml)}</dummy>.toString()
    assert(ldr == "t")
    assert(matSpec == "||||| |||||||| ||")
    XML.loadString(gndContent) should equal (loadResult("gndcontent.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("no-callnumber.xml")
    val ldr2 = cmiMapper.getFormat(recordAsXml2)
    val matSpec2 = cmiMapper.getMaterialSpecific(recordAsXml2)
    assert(ldr2 == "k")
    assert(matSpec2 == "nnn |     |    ||")

    val recordAsXml3 = loadRecord("person-no-main.xml")
    val ldr3 = cmiMapper.getFormat(recordAsXml3)
    val matSpec3 = cmiMapper.getMaterialSpecific(recordAsXml3)
    assert(ldr3 == "t")
    assert(matSpec3 == "||||| |||||||| ||")

    val recordAsXml4 = loadRecord("foto.xml")
    val ldr4 = cmiMapper.getFormat(recordAsXml4)
    val matSpec4 = cmiMapper.getMaterialSpecific(recordAsXml4)
    assert(ldr4 == "g")
    assert(matSpec4 == "||| |     |    f|")
  }

  test("links") {
    val recordAsXml = loadRecord("link-doi.xml")
    val link = <dummy>{cmiMapper.getLinkDigitized(recordAsXml)}</dummy>.toString()
    val doi = cmiMapper.getDoi(recordAsXml).toString()

    XML.loadString(link) should equal(loadResult("link.xml"))(after being streamlined[Elem])
    XML.loadString(doi) should equal(loadResult("doi.xml"))(after being streamlined[Elem])

    val recordAsXml2 = loadRecord("findingaid.xml")
    val result = {cmiMapper.getLinkSupplements(recordAsXml2)}.toString()

    XML.loadString(result) should equal(loadResult("linkSupplement.xml"))(after being streamlined[Elem])

  }

  test ("remove url")
  {
    val recordAsXml = loadRecord("findingaid.xml")
    val result = {cmiMapper.getField(recordAsXml, "FindingAid", "555##a")}.toString()

    XML.loadString(result) should equal(loadResult("remove-url.xml"))(after being streamlined[Elem])

    val recordAsXml2 = loadRecord("languages.xml")
    val result2 = {cmiMapper.getField(recordAsXml2, "FindingAid", "555##a")}.toString()
    XML.loadString(result2) should equal(loadResult("remove-url-2.xml"))(after being streamlined[Elem])

  }

  test ("holding") {
    val recordAsXml = loadRecord("bestand.xml")
    val result = {cmiMapper.getHolding(recordAsXml)}.toString()
    XML.loadString(result) should equal(loadResult("bestand-holding.xml"))(after being streamlined[Elem])

    val recordAsXml2 = loadRecord("places.xml")
    val result2 = {cmiMapper.getHolding(recordAsXml2)}.toString()
    XML.loadString(result2) should equal(loadResult("places-holding.xml"))(after being streamlined[Elem])
  }

}
