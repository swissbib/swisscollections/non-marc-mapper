/*
 * non-marc-mapper
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.Mappers.DublinCore.EPeriodicaMapper
import ch.swisscollections.Mappers.Mods.ERaraMapper
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import scala.io.Source
import scala.xml.{Elem, XML}


class ERaraMapperTest extends AnyFunSuite {
  lazy val eRaraMapper = new ERaraMapper

  def loadRecord(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/e-rara/record/$filename")
    val record = file.mkString
    val recordAsXml = XML.loadString(record)
    recordAsXml
  }
  def loadResult(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/e-rara/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }


  ignore("first mapping test") {
    val recordAsXml = loadRecord("25583780_mods.xml")
    val mappedRecord = eRaraMapper.mapToXmlMarc(recordAsXml)
    println(mappedRecord)
    mappedRecord should equal (loadResult("result-25583780-mods.xml")) (after being streamlined[Elem])

  }

  test("title") {
    val recordAsXml = loadRecord("27326477.xml")
    val result = eRaraMapper.getMainTitle(recordAsXml).toString()

    XML.loadString(result) should equal (loadResult("title.xml")) (after being streamlined[Elem])
  }

  test("publication statement") {
    val recordAsXml = loadRecord("12814965.xml")
    val result = eRaraMapper.getPublicationStatement(recordAsXml).toString()

    XML.loadString(result) should equal (loadResult("publicationStatement.xml")) (after being streamlined[Elem])
  }

  test("authors") {
    val recordAsXml = loadRecord("27326477.xml")
    val result = <dummy>{eRaraMapper.getAuthors(recordAsXml)}</dummy>.toString()

    XML.loadString(result) should equal (loadResult("authors.xml")) (after being streamlined[Elem])
  }

  test("topics") {
    val recordAsXml = loadRecord("27326477.xml")
    val result = <dummy>{eRaraMapper.getTopics(recordAsXml)}</dummy>.toString()

    XML.loadString(result) should equal (loadResult("topics.xml")) (after being streamlined[Elem])
  }

  test("superordinates") {
    val recordAsXml = loadRecord("24725611.xml")
    val result = {eRaraMapper.getHost(recordAsXml)}.toString()

    XML.loadString(result) should equal (loadResult("host.xml")) (after being streamlined[Elem])

    val recordAsXml2 = loadRecord("24754212.xml")
    val result2 = {eRaraMapper.getSeries(recordAsXml2)}.toString()

    XML.loadString(result2) should equal (loadResult("series.xml")) (after being streamlined[Elem])
  }



}
