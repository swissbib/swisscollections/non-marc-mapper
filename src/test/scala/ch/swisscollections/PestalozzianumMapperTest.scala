/*
 * non-marc-mapper
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.Mappers.DublinCore.PestalozzianumMapper
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import scala.io.Source
import scala.xml.{Elem, XML}


class PestalozzianumMapperTest extends AnyFunSuite {
  lazy val pestalozzianumMapper = new PestalozzianumMapper

  def loadRecord(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/pestalozzianum/record/$filename")
    val record = file.mkString
    val recordAsXml = XML.loadString(record)
    recordAsXml
  }
  def loadResult(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/pestalozzianum/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }


  test("first mapping test") {
  //ignore it as there is the processing date which changes in 008 field
    val recordAsXml = loadRecord("zeichnungen.xml")
    val mappedRecord = pestalozzianumMapper.mapToXmlMarc(recordAsXml)
println(mappedRecord)
    val mappedRecordParsedAgain = XML.loadString(mappedRecord.toString())
    //mappedRecordParsedAgain should equal (loadResult("result-aip4.xml")) (after being streamlined[Elem])

  }
}
