/*
 * non-marc-mapper
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.Mappers.Mods.ScriptaPaedagogicaMapper
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import scala.List
import scala.io.Source
import scala.xml.Utility.trim
import scala.xml.{Elem, NodeSeq, XML}


class ScriptaPaedagogicaMapperTest extends AnyFunSuite {
  lazy val scriptaPaedagogicaMapper = new ScriptaPaedagogicaMapper

  def loadRecord(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/scriptaPaedagogica/record/$filename")
    val record = file.mkString
    val recordAsXml = XML.loadString(record)
    recordAsXml
  }
  def loadSplittedRecord(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/scriptaPaedagogica/splitted_record/$filename")
    val record = file.mkString
    val recordAsXml = XML.loadString(record)
    recordAsXml
  }

  def loadResult(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/scriptaPaedagogica/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }

  test("map individual record") {
    val recordAsXml = loadRecord("paedzts_no-issue.xml")
    val res = scriptaPaedagogicaMapper.createIndividualRecord(recordAsXml, "DMDLOG_0020")
    val mappedRecord = scriptaPaedagogicaMapper.mapToXmlMarc(res)
    //println(mappedRecord)
  }


  test("first mapping test") {
    val recordAsXml = loadSplittedRecord("bericht-splitted.xml")
    val mappedRecord = scriptaPaedagogicaMapper.mapToXmlMarc(recordAsXml)
    println(mappedRecord)
    //mappedRecord should equal (loadResult("result.xml")) (after being streamlined[Elem])

  }

  test ("identifier") {
    val recordAsXml = loadRecord("zeitschrift.xml")
    val res = scriptaPaedagogicaMapper.createIndividualRecord(recordAsXml, "DMDLOG_0029")
    val identifier = <controlfield tag="001">BBF{(res \ "header" \ "identifier").text}</controlfield>

    assert(identifier.toString() == "<controlfield tag=\"001\">BBF025763679_0001_0029</controlfield>")
  }

  test("links") {
    val recordAsXml = loadRecord("zeitschrift.xml")
    val res = scriptaPaedagogicaMapper.createIndividualRecord(recordAsXml, "DMDLOG_0029")
    val mappedRecord = scriptaPaedagogicaMapper.mapToXmlMarc(res).toString().replaceAll("\\s", "")
    val expectedUrn = loadResult("links.xml").toString().replaceAll("\\s", "")
    val expectedPdf = loadResult("links-pdf.xml").toString().replaceAll("\\s", "")

    assert(mappedRecord contains expectedUrn)
    assert(mappedRecord contains expectedPdf)
  }

  ignore ("host") {

  }

  test("get relevant id's zeitschrift") {
    val recordAsXml = loadRecord("zeitschrift.xml")
    val idList = scriptaPaedagogicaMapper.getListofRelevantDmdId(recordAsXml)
    assert (idList.size == 42)
   }

  test("get relevant id's monographie") {
    val recordAsXml = loadRecord("oberstufe_monographie.xml")
    val idList = scriptaPaedagogicaMapper.getListofRelevantDmdId(recordAsXml)
    assert (idList.size == 1)
  }

  test("get relevant id's bericht") {
    val recordAsXml = loadRecord("bericht.xml")
    val idList = scriptaPaedagogicaMapper.getListofRelevantDmdId(recordAsXml)
    assert (idList.size == 1)
  }

  test("get relevant id's lexikon") {
    val recordAsXml = loadRecord("abendgymnasium_lexikon.xml")
    val idList = scriptaPaedagogicaMapper.getListofRelevantDmdId(recordAsXml)
    assert (idList.size == 374)
  }

  test("get parent id's") {
    val recordAsXml = loadRecord("zeitschrift.xml")
    //val idList = scriptaPaedagogicaMapper.getListofRelevantDmdId(recordAsXml)
    //val rec = (recordAsXml \\ "div").find(_.attribute("DMDID").exists(_.text == "DMDLOG_0029")).getOrElse("")
    val res = scriptaPaedagogicaMapper.getParents(recordAsXml, "DMDLOG_0029")
    println(res)

  }

  test("is parent of") {
    val recordAsXml = loadRecord("zeitschrift.xml")

    val structMap = scriptaPaedagogicaMapper.getStructMapLogical(recordAsXml)

    val node39 = scriptaPaedagogicaMapper.getNodeBasedOnDmdid(structMap, "DMDLOG_0039").get

    val node01 = scriptaPaedagogicaMapper.getNodeBasedOnDmdid(structMap, "DMDLOG_0001").get

    val node27 = scriptaPaedagogicaMapper.getNodeBasedOnDmdid(structMap, "DMDLOG_0027").get

    assert (scriptaPaedagogicaMapper.isParentOf(node39, "DMDLOG_0048") == true)
    assert (scriptaPaedagogicaMapper.isParentOf(node39, "DMDLOG_0001") == false)
    assert (scriptaPaedagogicaMapper.isParentOf(node39, "DMDLOG_0038") == false)

    assert (scriptaPaedagogicaMapper.isParentOf(node01, "DMDLOG_0029") == true)
    assert (scriptaPaedagogicaMapper.isParentOf(node27, "DMDLOG_0029") == true)
    assert (scriptaPaedagogicaMapper.isParentOf(node39, "DMDLOG_0029") == false)
  }

  test("get parents") {
    val recordAsXml = loadRecord("zeitschrift.xml")
    val res = scriptaPaedagogicaMapper.getParents(recordAsXml, "DMDLOG_0029")
    assert(res == List("DMDLOG_0001", "DMDLOG_0027"))

    val res2 = scriptaPaedagogicaMapper.getParents(recordAsXml, "DMDLOG_0027")
    assert(res2 == List("DMDLOG_0001"))

    val res3 = scriptaPaedagogicaMapper.getParents(recordAsXml, "DMDLOG_0001")
    assert(res3 == List())

    val res4 = scriptaPaedagogicaMapper.getParents(recordAsXml, "DMDLOG_0023")
    assert(res4 == List("DMDLOG_0001", "DMDLOG_0015"))

    val res5 = scriptaPaedagogicaMapper.getParents(recordAsXml, "DMDLOG_0015")
    assert(res5 == List("DMDLOG_0001"))

  }

  test("create individual record") {
    val recordAsXml = loadRecord("zeitschrift.xml")

    val res = scriptaPaedagogicaMapper.createIndividualRecord(recordAsXml, "DMDLOG_0029")

    //needs to do trim and toString to have the test working
    assert (trim(res).toString() == trim(loadSplittedRecord("zeitschrift-artikel.xml")).toString())
  }

  test("get first dmdsec id") {
    val recordAsXml = loadRecord("bericht.xml")

    val res = scriptaPaedagogicaMapper.getFirstDmdSecId(recordAsXml)

    assert (res == "DMDLOG_0001")
  }

  test("split records") {
    val recordAsXml = loadRecord("bericht.xml")
    val records = scriptaPaedagogicaMapper.splitRecord("kafka_message_key", recordAsXml)
    for ((key, record) <- records) {
      println("---------------------------")
      println(key)
      println(record)
    }


  }

}
